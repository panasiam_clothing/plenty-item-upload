# Plentymarkets Item Upload

## Introduction

A tool for the creation of upload files for the Plentymarkets Import service from Amazon flatfiles.  
The basic idea behind this approach is that the creation of multiple similar variations for an item via the Plentymarkets UI is quite time consuming.  
Amazon Flatfiles allow the user to easily reuse multiple texts and attributes for dozens of variations.  
Additionally, this procedure eases the initial work to import existing items on Amazon into Plentymarkets.  
Currently, this routine is optimized for uploading single items with multiple variations or to modify multiple variations from multiple parents.  
Uploading multiple new items is not supported right now.

## Installation

The project is hosted on GitLab, here are the install instructions for Linux (Ubuntu/Debian) and Windows10.

**Linux (Ubuntu/Debian)**:
```bash
# Dependencies
sudo apt update && sudo apt install git python3 python3-tk python3-pip
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
# Download and install
git clone https://gitlab.com/panasiam_clothing/plenty-item-upload.git
cd plenty-item-upload
poetry build
python3 -m pip install --upgrade pip && python3 -m pip install .
```

**Windows**:
- Download & install a recent version of Python from [here](https://www.python.org/downloads/windows/)
- Download & install a recent version of Git from [here](https://git-scm.com/download/win)
- Clone the project and install:
```bash
git clone https://gitlab.com/panasiam_clothing/plenty-item-upload.git
cd plenty-item-upload
python -m pip install --upgrade pip && python -m pip install poetry
poetry build
python -m pip install .
```

*Troubleshooting Windows*:  
In case you get the following error while building `cchardet`: **error: Microsoft Visual C++ 14.0 is required.**
Download the [Visual Studio Build Tools](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2017) and select the following individual components:
- Latest MSVC VS 201X C++ x64/x86 build tools
- Windows 10 SDK
- C++ CMake tools for Windows
- C++/CLI support

And try again.

### Using the Application dashboard

You can also manage this application via our [Application Dashboard](https://gitlab.com/panasiam_clothing/app-dashboard).

Here is an example configuration for that tool:

```json
{
    "applications": [
        {
            "name": "Item Upload",
            "type": "git",
            "url": "https://gitlab.com/panasiam_clothing/plenty-item-upload.git",
            "branch": "master",
            "remote": "origin",
            "local_path": "/home/user/plenty-item-upload",
            "pip_name": "item_upload",
            "configuration_path": "/home/user/.config/item_upload/config.ini",
            "parameter_configuration": [
                {
                    "parameter_name": "config",
                    "display_name": "Configuration folder",
                    "type": "path"
                },
                {
                    "parameter_name": "input",
                    "display_name": "Input data folder",
                    "type": "path"
                },
                {
                    "parameter_name": "output",
                    "display_name": "Output data folder",
                    "type": "path"
                }
            ]
        }
    ]
}
```

## Configuration

### Configuration with Plentymarkets

In this section you have to configure, which attributes to use as color and size (*to build the Plentymarkets attribute format string (`{color attribute backend name}:{color name};{size attribute backend name}:{size name}; ...` etc.)*), the base URL of your Plentymarkets REST API endpoint (*to connect with the REST API (the credentials are stored within your systems keyring)*) and the IDs of Amazon related barcodes.

Example:  
```ini
[PLENTY]
base_url = https://company.plentymarkets-cloud01.com
color_attribute_id = 1
size_attribute_id = 2
color_attribute_lang = en
selection_value_lang = en
asin_country_id = 0
fnsku_barcode_id = 3
```

### Configuration of available Categories

Plentymarkets demands the categories as a comma-separated string of category IDs. The `CATEGORY` mapping is used to connect these IDs with user friendly names for the GUI option menu.

Example:  
```ini
[CATEGORY]
men = 26
men.trousers = 27
men.shirts = 34
women = 37
women.trousers = 39
```

### Configuration of flatfile columns to Plentymarkets properties and features

One major feature of this tool is to ease the creation of features and properties, these can be quite complex as there are multiple types of features and properties with different demands for the upload file format.

There are three sections for mapping columns from the flatfile to Plentymarkets features/properties.  
- *PROPERTY* (item related properties)
- *FEATURES* (variation related properties)
- *SELECTION* (subtype of features, with fixed options, where the ID of the selection must be used within the upload file instead of the value)

Each contains mappings of column names to IDs.

Example:  
```ini
[PROPERTY]
material_type = 12

[FEATURES]
model = 4

[SELECTION]
lifestyle = 72
```

Explanation: The column `material_type` is mapped to the property on Plentymarkets with ID 12. The column `model` is mapped to the feature with ID 4 which contains text/number values and column `lifestyle` is mapped to Feature ID 72 which contains selection values.
The application will pull all selection values from the REST API to find the proper selection ID for each value in the flatfile column.

## Usage

The application demands the flatfile as semicolon separated value file (CSV). Place that file into the input folder (see *Modifying standard locations* below).

### Running the program

```bash
python -m item_upload
```

You can either choose the flatfile within the graphical user interface or set it via the CLI:

```bash
python -m item_upload --flatfile /path/to/flatfile.csv
```

### Modifying standard locations

By default the configuration will be placed into configuration folder of your operating system on Linux mostly `/home/$USER/.config/item_upload` and on Windows `C:/Users/$USER/AppData/Local/item_upload/item_upload`. While the input and output folders are placed into the documents folder of your system, on Linux `/home/$USER/Documents/item_upload` and on Windows `C:/Users/$USER/Documents/item_upload`.
You can modify these locations via the command line arguments.

```bash
python -m item_upload\
    --config /path/to/config_folder\
    --input /path/to/input_folder\
    --output /path/to/output_folder
```
