"""
Plentymarkets Item upload
Copyright (C) 2022 Sebastian Fricke Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from configparser import ConfigParser
import csv
import cchardet
import numpy as np
import pandas


# Mappings for Amazon Flatfile columns
TYPE_FIELD = 'feed_product_type'
SKU_FIELD = 'item_sku'
EAN_FIELD = 'external_product_id'
NAME_FIELD = 'item_name'
PRICE_FIELD = 'standard_price'
COLOR_FIELD = 'color_name'
SIZE_FIELD = 'size_name'
PSKU_FIELD = 'parent_sku'
RELATION_FIELD = 'parent_child'
DESC_FIELD = 'product_description'
KEYWORD_FIELD = 'generic_keywords'
LENGTH_FIELD = 'package_length'
WIDTH_FIELD = 'package_width'
HEIGHT_FIELD = 'package_height'
WEIGHT_FIELD = 'package_weight'
ITEM_WEIGHT_FIELD = 'item_weight'


class FlatfileService:
    def __init__(self, config: ConfigParser, flatfile: str) -> None:
        self.config = config
        self.flatfile = flatfile
        self.encoding = self.get_encoding()
        self.sep = self.get_separator()

    def get_encoding(self) -> str:
        if not self.flatfile:
            return ''
        with open(self.flatfile, mode='rb') as flatfile:
            raw_data = flatfile.read()
            return cchardet.detect(raw_data)['encoding']

    def get_separator(self) -> str:
        if not self.encoding:
            return ''
        with open(self.flatfile, mode='r', encoding=self.encoding) as flatfile:
            try:
                sep = csv.Sniffer().sniff(
                    flatfile.readline(), [';', ',', '\t']
                ).delimiter
            except csv.Error as err:
                raise RuntimeError(
                    'No valid separator detected in the flatfile at '
                    f'{self.flatfile}'
                ) from err
            flatfile.seek(0)
        return sep

    def get_header_row(self) -> int:
        static_valid_values = [
            'item_sku', 'external_product_id', 'variation_theme', 'parent_sku'
        ]
        dataframe: pandas.DataFrame = pandas.read_csv(
            self.flatfile, sep=';', nrows=4
        )
        if all(valid in dataframe.columns for valid in static_valid_values):
            return 0
        if all(
            valid in dataframe.loc[1].to_list()
            for valid in static_valid_values
        ):
            return 2
        raise RuntimeError(
            'The flatfile must have a valid header file at either the first '
            f'or third row\n(Searching for: [{static_valid_values}]'
        )

    def read_flatfile(self) -> pandas.DataFrame:
        if not self.encoding or not self.sep:
            return pandas.DataFrame()
        header_row = self.get_header_row()
        dataframe: pandas.DataFrame = pandas.read_csv(
            self.flatfile, sep=self.sep, header=header_row,
            encoding=self.encoding, dtype=str
        )
        dataframe.replace(np.nan, '', regex=True, inplace=True)
        # Check if parents are found
        if len(dataframe[dataframe[RELATION_FIELD] == 'parent'].index) == 0:
            raise RuntimeError(
                "No parents found within the flatfile, parent variations"
                " are required to properly group child variations, to "
                "assign position numbers and to fill missing prices."
            )
        return dataframe
