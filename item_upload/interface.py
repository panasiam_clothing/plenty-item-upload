"""
Plentymarkets Item upload
Copyright (C) 2022  Sebastian Fricke Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from argparse import Namespace
from collections import defaultdict
from configparser import ConfigParser
from functools import partial
import itertools
import os
from pathlib import Path
import tkinter
import tkinter.ttk
import tkinter.filedialog
from typing import Callable, Dict, List, TypedDict, Union
from pandas.core.frame import DataFrame
from plenty_api.api import PlentyApi
from loguru import logger

from item_upload.plentymarkets_connector import (
    AttributeService, BarcodeService, SelectionFeatureService
)
from item_upload.flatfile_handling import FlatfileService
from item_upload.file_creation import FileCreationService, CustomData


# Dimensions
WINDOW_WIDTH = 700
WINDOW_HEIGHT = 500

BUTTON_WIDTH = 150
BUTTON_HEIGHT = 50
NEXT_BUTTON_X = WINDOW_WIDTH / 10
BACK_BUTTON_X = (WINDOW_WIDTH / 10) * 9 - BUTTON_WIDTH
BOTTOM_ROW_BUTTON_Y = WINDOW_HEIGHT - 1.5 * BUTTON_HEIGHT
TOP_SEPARATOR_Y = 60
BOTTOM_SEPARATOR_Y = BOTTOM_ROW_BUTTON_Y - 30

SMALL_FONT_SIZE = 8
NORMAL_FONT_SIZE = 12
STATUS_FONT_SIZE = 18
HEADER_FONT_SIZE = 25

# Colors
FRAME_COLOR = '#f4cccc'
TEXT_COLOR = '#000000'
ENTRY_COLOR = '#ffffff'
BUTTON_COLOR = '#fffaed'
ACTIVE_BUTTON_COLOR = '#cfe2f3'
ALTERNATIVE_BUTTON_COLOR = '#99968E'
ALTERNATIVE_ACTIVE_BUTTON_COLOR = '#7C8791'
ALTERNATIVE_TEXT_COLOR = '#fffaed'
ERROR_COLOR = '#ffb6a1'

# Fonts
HEADER_FONT = ('Helvetica Bold', HEADER_FONT_SIZE)
HIGHLIGHT_FONT = ('Helvetica Bold', NORMAL_FONT_SIZE, 'bold')
NORMAL_FONT = ('Helvetica', NORMAL_FONT_SIZE)
NOTE_FONT = ('Helvetica', SMALL_FONT_SIZE, 'italic')
PROPERTY_HEADER_FONT = ('Helvetica', 10, 'underline')
SMALL_NOTE_FONT = ('Helvetica', SMALL_FONT_SIZE)
STATUS_FONT = ('Helvetica Bold', STATUS_FONT_SIZE)


class Command(TypedDict):
    title: str
    command: Callable


class Interface:
    def __init__(
        self, config: ConfigParser, args: Namespace, folders: Dict[str, Path]
    ) -> None:
        self.config = config
        self.args = args
        self.folders = folders
        self.root = tkinter.Tk()
        self.root.wm_title('Item upload')
        self.root.geometry(f'{WINDOW_WIDTH}x{WINDOW_HEIGHT}')
        self.root.resizable(width=False, height=False)

        self.api = self.__setup_plenty_api()
        self.attribute_service = AttributeService(
            config=self.config, api_instance=self.api
        )
        self.selection_service = SelectionFeatureService(
            config=self.config, api_instance=self.api
        )
        self.barcode_service = BarcodeService(
            config=self.config, api_instance=self.api
        )
        self.dataframe: DataFrame = DataFrame()

        self.container = tkinter.Frame(
            self.root, width=WINDOW_WIDTH, height=WINDOW_HEIGHT,
            background=FRAME_COLOR
        )
        self.container.place(
            x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT
        )
        self.frames = {}
        self.__set_up_pages()

        self.flatfile: str = ''
        if args.flatfile and args.flatfile.exists():
            self.read_flatfile(flatfile=args.flatfile)
        else:
            self.show_frame('FLATFILE')

    def read_flatfile(self, flatfile: str = '') -> None:
        if not flatfile and self.flatfile:
            flatfile = self.flatfile
        self.flatfile = flatfile
        flatfile_service = FlatfileService(
            config=self.config, flatfile=flatfile
        )
        dataframe = flatfile_service.read_flatfile()
        if len(dataframe.index) == 0:
            return
        self.dataframe = dataframe
        self.missing_color_check()

    def __setup_plenty_api(self) -> PlentyApi:
        api = PlentyApi(self.config.get(section='PLENTY', option='base_url'))
        api.cli_progress_bar = True
        return api

    def missing_color_check(self, refresh: bool = False) -> None:
        assert len(self.dataframe.index) > 0
        frame = self.frames['COLOR_CHECK']
        self.__loading_screen()
        self.attribute_service.pull_color_attributes(overwrite_cache=refresh)
        missing = self.attribute_service.generate_missing_list(
            flatfile=self.dataframe
        )
        if missing:
            frame.missing_colors = missing
            frame.build_color_view_frame()
            self.show_frame('COLOR_CHECK')
        else:
            self.missing_selections_check()

    def missing_selections_check(self, refresh: bool = False) -> None:
        assert len(self.dataframe.index) > 0
        frame = self.frames['SELECTION_CHECK']
        self.__loading_screen()
        self.selection_service.pull_property_names(overwrite_cache=refresh)
        self.selection_service.pull_selections(overwrite_cache=refresh)
        missing = self.selection_service.generate_missing_dictionary(
            flatfile=self.dataframe
        )
        if missing:
            frame.missing_selections = missing
            frame.build_selection_view_frame()
            self.show_frame('SELECTION_CHECK')
        else:
            self.missing_amazon_identification_check()

    def missing_amazon_identification_check(
        self, refresh: bool = False
    ) -> None:
        assert len(self.dataframe.index) > 0
        frame = self.frames['IDENTIFICATION_CHECK']
        self.__loading_screen()
        self.barcode_service.pull_barcodes(overwrite_cache=refresh)
        variations = self.barcode_service.get_data_for_flatfile(
            flatfile=self.dataframe
        )
        if variations['asin'].all() and variations['fnsku'].all():
            self.show_frame('ITEM_DATA')
        else:
            frame.variations = variations
            frame.build_variation_view_frame()
            self.show_frame('IDENTIFICATION_CHECK')

    def create_files(self) -> None:
        file_creation_service = FileCreationService(
            config=self.config, flatfile=self.dataframe,
            upload_folder=self.folders['output']
        )
        self.show_frame('LOADING')
        name = self.frames['ITEM_DATA'].get()['name1']
        file_creation_service.barcode_service.custom_mapping =\
            dict(self.frames['IDENTIFICATION_CHECK'].custom_mapping)
        file_creation_service.set_file_ending(name=name)
        commands: List[Command] = []
        commands.append(
            {
                'title': 'Create an item upload file',
                'command': partial(
                    file_creation_service.item_upload,
                    self.frames['ITEM_DATA'].get()
                )
            }
        )

        commands.append(
            {
                'title': 'Create a property upload file',
                'command': partial(
                    file_creation_service.create_property_upload_file,
                    self.create_mapping(section='PROPERTY')
                )
            }
        )

        selection_mapping = self.selection_service.create_selection_mapping(
            flatfile=self.dataframe,
            user_mapping=self.frames['SELECTION_CHECK'].custom_mapping
        )
        configuration_mapping = {
            'text': self.create_mapping(section='FEATURES'),
            'selection': self.create_mapping(section='SELECTION')
        }
        commands.append(
            {
                'title': 'Create a feature upload file',
                'command': partial(
                    file_creation_service.create_feature_upload_file,
                    selection_mapping, configuration_mapping
                )
            }
        )
        self.show_frame('EXECUTION')
        self.frames['EXECUTION'].execute(actions=commands)

    def __loading_screen(self) -> None:
        self.show_frame('LOADING')
        self.container.update()

    def __set_up_pages(self) -> None:
        pages = [
            ('LOADING', LoadingFrame),
            ('FLATFILE', FlatfileChooserFrame),
            ('COLOR_CHECK', MissingColorCheckFrame),
            ('SELECTION_CHECK', MissingSelectionFrame),
            ('IDENTIFICATION_CHECK', MissingIdentificationFrame),
            ('ITEM_DATA', ItemDataFrame),
            ('EXECUTION', ExecutionFrame)
        ]
        for name, page in pages:
            frame = page(self)
            self.frames[name] = frame
            frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)

    def show_frame(self, frame: str) -> None:
        """
        Move the specified frame to the top of the frame stack maintained by
        tkinter (active window for the user).

        Parameters:
            frame        [str]      -       Frame that is moved up in stack
        """
        self.frames[frame].tkraise()

    def create_mapping(self, section: str) -> Union[Dict[str, int], None]:
        """
        Create a mapping of specific texts to Plentymarkets IDs from a certain
        section of the ini configuration file.

        This is currently used to map category frontend names to category IDs
        for the GUI and to map flatfile columns to property/feature IDs.

        Parameters:
            section         [str]           -       section in the config the
                                                    mapping will be created for

        Return:
                            [dict]          -       dictionary containing the
                                                    mapping
        """
        mapping = {}

        if section not in self.config.keys():
            return None

        for key, value in self.config[section].items():
            try:
                mapping[key] = int(value)
            except ValueError:
                logger.error(
                    f"Incorrect ID in config for => {key}:{value} in {section}"
                )

        return mapping


class LoadingFrame(tkinter.Frame):
    """ Loading screen to indicate that the application is currently busy """
    def __init__(self, parent: Interface) -> None:
        tkinter.Frame.__init__(self, parent.container, background=FRAME_COLOR)
        self.parent = parent
        tkinter.Label(
            self, text='Loading ...', font=HEADER_FONT, background=FRAME_COLOR,
            foreground=TEXT_COLOR
        ).place(
            x=WINDOW_WIDTH / 2 - 150, y=WINDOW_HEIGHT / 2 - 20, width=300,
            height=40
        )

class CustomFrame(tkinter.Frame):
    """ Base class for each page that adds a centered title """
    def __init__(self, parent, title) -> None:
        tkinter.Frame.__init__(self, parent.container, background=FRAME_COLOR)
        self.parent = parent
        tkinter.Label(
            self, text=title, font=HEADER_FONT, justify='center',
            background=FRAME_COLOR, foreground=TEXT_COLOR
        ).place(
            x=WINDOW_WIDTH / 2 - (len(title) * HEADER_FONT_SIZE // 2), y=20,
            width=len(title) * HEADER_FONT_SIZE, height=30)
        tkinter.ttk.Separator(
            self, orient='horizontal'
        ).place(x=0, y=TOP_SEPARATOR_Y, width=WINDOW_WIDTH, height=2)
        tkinter.ttk.Separator(
            self, orient='horizontal'
        ).place(x=0, y=BOTTOM_SEPARATOR_Y, width=WINDOW_WIDTH, height=2)


class LabeledEntry(tkinter.Frame):
    """ Entry widget with a description on the right side """
    def __init__(self, parent, label_text: str, label_len: int,
                 entry_len: int, font_size: int = 10) -> None:
        tkinter.Frame.__init__(self, parent, background=FRAME_COLOR)
        self.label = tkinter.Label(
            self, text=label_text, font=('Helvetica', font_size),
            background=FRAME_COLOR, foreground=TEXT_COLOR
        )
        self.label.place(x=0, y=0, width=label_len, height=20)
        self.entry_text = tkinter.StringVar()
        self.entry = tkinter.Entry(
            self, textvariable=self.entry_text,
            background=ENTRY_COLOR, foreground=TEXT_COLOR
        )
        self.entry.place(x=label_len + 5, y=0, width=entry_len, height=20)

    def get(self):
        """ Retrieve the content of the text field """
        return self.entry_text.get()

    def set(self, text):
        """ Set the content of the text field """
        self.entry_text.set(text)


class LabeledOptionMenu(tkinter.Frame):
    """ OptionMenu widget with a description on top """
    def __init__(self, parent, label_text: str, options: List[str],
                 menu_width: int = 200) -> None:
        tkinter.Frame.__init__(self, parent, background=FRAME_COLOR)

        self.label = tkinter.Label(
            self, text=label_text, justify='left', background=FRAME_COLOR,
            foreground=TEXT_COLOR, font=NORMAL_FONT
        )
        self.options = options
        self.option_value = tkinter.StringVar()
        self.option_menu = tkinter.OptionMenu(
            self, self.option_value, *options
        )
        self.option_value.set(options[0])
        self.label.place(x=0, y=0, width=menu_width, height=25)
        self.option_menu.place(x=0, y=25, width=menu_width, height=25)

    def get(self):
        """ Retrieve the current selection from the option menu """
        return self.option_value.get()


class ScrollableFrame(tkinter.Frame):
    """
    Implementation of a hacky workaround to Tkinters inabillity to apply a
    scrollbar to a regular frame widget.
    The workaround involves adding a canvas to the frame and adding a frame
    widget to the canvas as window.
    One drawback that I discovered so far, is that I cannot add another frame
    to `self.frame` instead I have to place widgets like labels and entry boxes
    directly to it.
    """
    def __init__(self, parent: tkinter.Frame) -> None:
        tkinter.Frame.__init__(self, parent, background=FRAME_COLOR)
        self._canvas = tkinter.Canvas(
            self, borderwidth=0, background=FRAME_COLOR,
            highlightthickness=0
        )
        self.frame = tkinter.Frame(self._canvas, background=FRAME_COLOR)
        self._vsb = tkinter.ttk.Scrollbar(
            self, orient='vertical', command=self._canvas.yview
        )
        self._canvas.configure(yscrollcommand=self._vsb.set)
        self._vsb.pack(side="right", fill="y")
        self._canvas.pack(side="left", fill="both", expand=True)
        self._canvas.create_window(
            (4, 4), window=self.frame, anchor='nw',
            tags='self.frame'
        )

        self.frame.bind("<Configure>", self.__on_frame_configure)
        self.frame.bind("<Enter>", self.__bound_to_mousewheel)
        self.frame.bind("<Leave>", self.__unbound_to_mousewheel)

    def __bound_to_mousewheel(self, event):
        del event
        if os.name == 'posix':
            self._canvas.bind_all("<Button-4>", self.__on_mousewheel)
            self._canvas.bind_all("<Button-5>", self.__on_mousewheel)
        else:
            self._canvas.bind_all("<MouseWheel>", self.__on_mousewheel)

    def __unbound_to_mousewheel(self, event):
        del event
        if os.name == 'posix':
            self._canvas.unbind_all("<Button-4>")
            self._canvas.unbind_all("<Button-5>")
        else:
            self._canvas.unbind_all("<MouseWheel>")

    def __on_mousewheel(self, event):
        scroll_value = 0
        if event.num == 5 or event.delta == -120:
            scroll_value = 1
        elif event.num == 4 or event.delta == 120:
            scroll_value = -1
        self._canvas.yview_scroll(scroll_value, "units")

    def __on_frame_configure(self, event):
        """Reset the scroll region to encompass the inner frame"""
        del event
        self._canvas.configure(scrollregion=self._canvas.bbox("all"))


class MissingColorCheckFrame(CustomFrame):
    def __init__(self, parent: Interface) -> None:
        CustomFrame.__init__(self, parent, 'Unknown color values')
        self.parent = parent
        self.attribute_service = parent.attribute_service
        self.flatfile: DataFrame
        self.color_view: ColorViewFrame = None
        self.ignored_colors = []
        self.created_colors = []
        self.missing_colors = []
        self.refresh_attributes = tkinter.Button(
            self, text='Refresh attributes', command=self.__refresh_attributes,
            background=ALTERNATIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR,
            activebackground=ALTERNATIVE_ACTIVE_BUTTON_COLOR,
            activeforeground=ALTERNATIVE_TEXT_COLOR
        )
        self.refresh_flatfile = tkinter.Button(
            self, text='Refresh flatfile', command=self.__refresh_flatfile,
            background=ALTERNATIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR,
            activebackground=ALTERNATIVE_ACTIVE_BUTTON_COLOR,
            activeforeground=ALTERNATIVE_TEXT_COLOR
        )
        self.create_all_button = tkinter.Button(
            self, text='Create all', command=self.__create_all_colors,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.create_selection_button = tkinter.Button(
            self, text='Create selection', command=self.__create_selected,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.ignore_all_button = tkinter.Button(
            self, text='Ignore all', command=self.__ignore_all_colors,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.ignore_selection_button = tkinter.Button(
            self, text='Ignore selection',
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR,
            command=self.__ignore_selected_colors
        )
        self.next_button = tkinter.Button(
            self, text='Next', command=parent.missing_selections_check,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.next_button['state'] = 'disabled'
        self.back_button = tkinter.Button(
            self, text='Back', command=partial(
                parent.show_frame, 'FLATFILE'
            ),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.blocked_notification = tkinter.Label(
            self, text='blocked until the list is empty',
            font=SMALL_NOTE_FONT, background=FRAME_COLOR,
            foreground=TEXT_COLOR
        )
        self.refresh_attributes.place(
            x=25, y=TOP_SEPARATOR_Y + 10, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT / 2
        )
        self.refresh_flatfile.place(
            x=25 + 1 * BUTTON_WIDTH, y=TOP_SEPARATOR_Y + 10,
            width=BUTTON_WIDTH, height=BUTTON_HEIGHT / 2
        )
        tkinter.ttk.Separator(
            self, orient='horizontal'
        ).place(
            x=0, y=TOP_SEPARATOR_Y + 15 + BUTTON_HEIGHT / 2,
            width=WINDOW_WIDTH, height=2
        )
        self.create_all_button.place(
            x=NEXT_BUTTON_X - 20, y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10,
            width=BUTTON_WIDTH, height=BUTTON_HEIGHT
        )
        self.create_selection_button.place(
            x=NEXT_BUTTON_X - 20 + BUTTON_WIDTH,
            y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.ignore_all_button.place(
            x=NEXT_BUTTON_X - 20 + 2 * BUTTON_WIDTH,
            y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.ignore_selection_button.place(
            x=NEXT_BUTTON_X - 20 + 3 * BUTTON_WIDTH,
            y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.next_button.place(
            x=NEXT_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.blocked_notification.place(
            x=NEXT_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y + BUTTON_HEIGHT + 5,
            width=BUTTON_WIDTH, height=10
        )
        self.back_button.place(
            x=BACK_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )

    def build_color_view_frame(self) -> None:
        """
        To ensure that we first fetch the attributes before building the frame
        """
        if self.color_view:
            self.color_view.place_forget()
        height = BOTTOM_SEPARATOR_Y - 1.5 * BUTTON_HEIGHT - 30 -\
            TOP_SEPARATOR_Y
        self.color_view = ColorViewFrame(parent=self)
        self.color_view.place(
            x=25, y=TOP_SEPARATOR_Y + BUTTON_HEIGHT / 2 + 20,
            width=WINDOW_WIDTH-60, height=height
        )
        if not self.color_view.color_rows:
            self.blocked_notification.place_forget()
            self.next_button['state'] = 'normal'

    def __refresh_attributes(self) -> None:
        self.parent.missing_color_check(refresh=True)

    def __refresh_flatfile(self) -> None:
        self.parent.read_flatfile()

    def __create_all_colors(self) -> None:
        self.parent.show_frame('LOADING')
        self.create_all_button.configure({'background': BUTTON_COLOR})
        try:
            if self.attribute_service.create_colors(
                colors=list(self.color_view.colors)
            ):
                self.created_colors = list(self.color_view.colors)
                self.__refresh_attributes()
            else:
                self.parent.show_frame('COLOR_CHECK')
        except RuntimeError as err:
            self.parent.show_frame('COLOR_CHECK')
            logger.warning(err)
            self.create_all_button.configure({'background': ERROR_COLOR})

    def __create_selected(self) -> None:
        selected = []
        self.parent.show_frame('LOADING')
        for row in self.color_view.color_rows:
            if row.selected.get():
                selected.append(row.name)
        self.create_selection_button.configure({'background': BUTTON_COLOR})
        try:
            if self.attribute_service.create_colors(colors=selected):
                self.created_colors = selected
                self.__refresh_attributes()
            else:
                self.parent.show_frame('COLOR_CHECK')
        except RuntimeError as err:
            self.parent.show_frame('COLOR_CHECK')
            logger.warning(err)
            self.create_selection_button.configure({'background': ERROR_COLOR})

    def __ignore_all_colors(self) -> None:
        self.ignored_colors = list(self.color_view.colors)
        self.build_color_view_frame()

    def __ignore_selected_colors(self) -> None:
        ignored = []
        for row in self.color_view.color_rows:
            if row.selected.get():
                ignored.append(row.name)
        self.ignored_colors = ignored
        self.build_color_view_frame()


class ColorViewFrame(ScrollableFrame):
    def __init__(self, parent: MissingColorCheckFrame) -> None:
        ScrollableFrame.__init__(self, parent)

        self.parent = parent
        self.attribute_service = parent.attribute_service
        self.color_rows = []
        self.colors = self.attribute_service.create_color_suggestions(
            colors=parent.missing_colors
        )
        self.populate()

    def populate(self):
        self.color_rows = []
        for index, color in enumerate(self.colors.items()):
            if (
                color[0] in self.parent.ignored_colors or
                color[0] in self.parent.created_colors
            ):
                continue
            row = ColorRow(
                parent=self, name=color[0], suggestions=color[1],
                row_index=index
            )
            self.color_rows.append(row)


class ColorRow:
    def __init__(
        self, parent: ColorViewFrame, name: str, suggestions: List[str],
        row_index: int
    ) -> None:
        self.parent = parent
        self.row_index = row_index
        self.name = name
        self.suggestions = suggestions
        self.selected = tkinter.BooleanVar()
        self.checkbox = tkinter.Checkbutton(
            self.parent.frame, variable=self.selected,
            background=FRAME_COLOR, foreground=TEXT_COLOR,
            activebackground=ACTIVE_BUTTON_COLOR,
            highlightthickness=0
        )
        self.label = tkinter.Label(
            self.parent.frame, text=f'[{name}]', background=FRAME_COLOR,
            foreground=TEXT_COLOR, font=HIGHLIGHT_FONT
        )
        if len(self.suggestions) == 0:
            tkinter.Label(
                self.parent.frame, text='No similar colors found',
                background=FRAME_COLOR, foreground=TEXT_COLOR,
                font=NOTE_FONT
            ).grid(row=row_index, column=2)
        else:
            self.option_value = tkinter.StringVar()
            self.option_menu = tkinter.ttk.Combobox(
                self.parent.frame, textvariable=self.option_value,
                exportselection=False, takefocus=False,
                values=self.suggestions, state='readonly'
            )
            self.option_menu.set(self.suggestions[0])
            self.save_button = tkinter.Button(
                self.parent.frame, text='Use color',
                command=self.__save,
                background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
                foreground=TEXT_COLOR
            )
            self.option_menu.grid(row=row_index, column=2)
            self.save_button.grid(row=row_index, column=3, padx=5)
        self.checkbox.grid(row=row_index, column=0, padx=10)
        self.label.grid(row=row_index, column=1, padx=20)

    def __save(self, *args):
        del args
        # Get the flatfile as pandas DataFrame from the Interface class
        flatfile = self.parent.parent.parent.dataframe
        current_choice = self.option_value.get()
        match = flatfile['color_name'] == self.name
        if not match.sum():
            logger.warning(
                f'Unable to locate {self.name} within the flatfile.'
            )
        flatfile.loc[flatfile.loc[match].index, 'color_name'] = current_choice
        self.parent.parent.missing_colors.remove(self.name)
        self.parent.parent.build_color_view_frame()


class MissingSelectionFrame(CustomFrame):
    def __init__(self, parent: Interface) -> None:
        CustomFrame.__init__(self, parent, 'Unknown selection values')
        self.parent = parent
        self.selection_service = parent.selection_service
        self.flatfile: DataFrame
        self.selection_view: SelectionViewFrame
        self.created_selections: List[str] = []
        self.skipped_selections: List[str] = []
        self.missing_selections: Dict[int, List[str]] = {}
        self.custom_mapping: Dict[str, int] = {}

        self.refresh_attributes = tkinter.Button(
            self, text='Refresh selections', command=self.__refresh_selections,
            background=ALTERNATIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR,
            activebackground=ALTERNATIVE_ACTIVE_BUTTON_COLOR,
            activeforeground=ALTERNATIVE_TEXT_COLOR
        )
        self.refresh_flatfile = tkinter.Button(
            self, text='Refresh flatfile', command=self.__refresh_flatfile,
            background=ALTERNATIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR,
            activebackground=ALTERNATIVE_ACTIVE_BUTTON_COLOR,
            activeforeground=ALTERNATIVE_TEXT_COLOR
        )
        self.create_all_button = tkinter.Button(
            self, text='Create all', command=self.__create_all_selections,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.create_selection_button = tkinter.Button(
            self, text='Create selection', command=self.__create_selected,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.skip_all_button = tkinter.Button(
            self, text='Skip all', command=self.__skip_all_selections,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.skip_selection_button = tkinter.Button(
            self, text='Skip selection', command=self.__skip_selected,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.next_button = tkinter.Button(
            self, text='Next',
            command=parent.missing_amazon_identification_check,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.next_button['state'] = 'disabled'
        self.back_button = tkinter.Button(
            self, text='Back', command=partial(
                parent.show_frame, 'FLATFILE'
            ),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.blocked_notification = tkinter.Label(
            self, text='blocked until the list is empty',
            font=SMALL_NOTE_FONT, background=FRAME_COLOR,
            foreground=TEXT_COLOR
        )
        self.refresh_attributes.place(
            x=25, y=TOP_SEPARATOR_Y + 10, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT / 2
        )
        self.refresh_flatfile.place(
            x=25 + 1 * BUTTON_WIDTH, y=TOP_SEPARATOR_Y + 10,
            width=BUTTON_WIDTH, height=BUTTON_HEIGHT / 2
        )
        tkinter.ttk.Separator(
            self, orient='horizontal'
        ).place(
            x=0, y=TOP_SEPARATOR_Y + 15 + BUTTON_HEIGHT / 2,
            width=WINDOW_WIDTH, height=2
        )
        self.create_all_button.place(
            x=NEXT_BUTTON_X - 20, y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10,
            width=BUTTON_WIDTH, height=BUTTON_HEIGHT
        )
        self.create_selection_button.place(
            x=NEXT_BUTTON_X - 20 + BUTTON_WIDTH,
            y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.skip_all_button.place(
            x=NEXT_BUTTON_X - 20 + 2 * BUTTON_WIDTH,
            y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.skip_selection_button.place(
            x=NEXT_BUTTON_X - 20 + 3 * BUTTON_WIDTH,
            y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.next_button.place(
            x=NEXT_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.blocked_notification.place(
            x=NEXT_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y + BUTTON_HEIGHT + 5,
            width=BUTTON_WIDTH, height=10
        )
        self.back_button.place(
            x=BACK_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )

    def build_selection_view_frame(self) -> None:
        self.selection_view = SelectionViewFrame(parent=self)
        height = BOTTOM_SEPARATOR_Y - 1.5 * BUTTON_HEIGHT - 30 -\
            TOP_SEPARATOR_Y
        self.selection_view.place(
            x=25, y=TOP_SEPARATOR_Y + BUTTON_HEIGHT / 2 + 20,
            width=WINDOW_WIDTH-60, height=height
        )
        if not self.selection_view.selection_rows:
            self.blocked_notification.place_forget()
            self.next_button['state'] = 'normal'

    def __create_all_selections(self) -> None:
        self.parent.show_frame('LOADING')
        if self.selection_service.create_selections(
            selections=self.missing_selections
        ):
            self.created_selections = list(
                itertools.chain.from_iterable(self.missing_selections.values())
            )
            self.__refresh_selections()
            self.build_selection_view_frame()
        else:
            self.parent.show_frame('SELECTION_CHECK')

    def __create_selected(self) -> None:
        self.parent.show_frame('LOADING')
        selected: Dict[int, List[str]] = defaultdict(list)
        for row in self.selection_view.selection_rows:
            if row.selected.get():
                selected[row.property_id].append(row.name)
        if self.selection_service.create_selections(selections=dict(selected)):
            self.created_selections = list(
                itertools.chain.from_iterable(selected.values())
            )
            self.__refresh_selections()
            self.build_selection_view_frame()
        else:
            self.parent.show_frame('SELECTION_CHECK')

    def __skip_all_selections(self) -> None:
        self.skipped_selections = list(
            itertools.chain.from_iterable(self.missing_selections.values())
        )
        self.build_selection_view_frame()

    def __skip_selected(self) -> None:
        selected: Dict[int, List[str]] = defaultdict(list)
        for row in self.selection_view.selection_rows:
            if row.selected.get():
                selected[row.property_id].append(row.name)
        self.skipped_selections = list(
            itertools.chain.from_iterable(selected.values())
        )
        self.build_selection_view_frame()

    def __refresh_flatfile(self) -> None:
        self.parent.read_flatfile()

    def __refresh_selections(self) -> None:
        self.parent.missing_selections_check(refresh=True)


class SelectionViewFrame(ScrollableFrame):
    def __init__(self, parent: MissingSelectionFrame) -> None:
        ScrollableFrame.__init__(self, parent)
        self.parent = parent
        self.frame.columnconfigure(index=1, weight=5)
        for index in [0, 2, 3]:
            self.frame.columnconfigure(index=index, weight=1)
        self.selection_service = parent.selection_service
        self.property_names = self.selection_service.pull_property_names()
        self.selection_rows = []
        index = 0
        hidden_values = list(
            itertools.chain(
                self.parent.skipped_selections,
                self.parent.created_selections,
                list(self.parent.custom_mapping)
            )
        )
        for property_id, selection in parent.missing_selections.items():
            match_id = self.property_names['property_id'] == property_id
            try:
                property_name = self.property_names[match_id]['name'].item()
            except ValueError:
                property_name = 'No name found'
            self.property_label = tkinter.Label(
                self.frame,
                text=f'Feature ID: {property_id} [{property_name}]',
                background=FRAME_COLOR, foreground=TEXT_COLOR,
                font=PROPERTY_HEADER_FONT
            )
            if any(value not in hidden_values for value in selection):
                self.property_label.grid(
                    row=index, column=0, columnspan=3, pady=10
                )
                index += 1
            for value in selection:
                if value in hidden_values:
                    continue
                row = SelectionRow(
                    parent=self, property_id=property_id, name=value,
                    row_index=index
                )
                index += 1
                self.selection_rows.append(row)

class SelectionRow:
    def __init__(
        self, parent: SelectionViewFrame, property_id: int, name: str,
        row_index: int
    ) -> None:
        self.parent = parent
        self.property_id = property_id
        self.name = name
        self.row_index = row_index
        self.selected = tkinter.BooleanVar()
        self.checkbox = tkinter.Checkbutton(
            self.parent.frame, variable=self.selected,
            background=FRAME_COLOR, foreground=TEXT_COLOR,
            highlightthickness=0
        )
        self.missing_value = tkinter.Label(
            self.parent.frame, text=name,
            background=FRAME_COLOR, foreground=TEXT_COLOR,
            font=HIGHLIGHT_FONT
        )
        api_data = self.parent.selection_service.selections
        matching_selections = api_data[api_data['property_id'] == property_id]
        if len(matching_selections.index) == 0:
            tkinter.Label(
                self.parent.frame, text='No existing options found',
                background=FRAME_COLOR, foreground=TEXT_COLOR,
                font=NOTE_FONT
            ).grid(row=row_index, column=2)
        else:
            self.options = matching_selections['value'].to_list()
            self.option_value = tkinter.StringVar()
            self.option_menu = tkinter.ttk.Combobox(
                self.parent.frame, textvariable=self.option_value,
                exportselection=False, takefocus=False, values=self.options,
                state='readonly'
            )
            self.option_menu.set(self.options[0])
            self.save_button = tkinter.Button(
                self.parent.frame, text='Use selection',
                command=self.__save,
                background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
                foreground=TEXT_COLOR
            )
            self.option_menu.grid(row=row_index, column=2)
            self.save_button.grid(row=row_index, column=3)
        self.checkbox.grid(row=row_index, column=0, padx=10)
        self.missing_value.grid(row=row_index, column=1, padx=20)

    def __save(self, *args) -> None:
        del args
        current = self.option_value.get()
        api_data = self.parent.selection_service.selections
        match = api_data[
            (api_data['property_id'] == self.property_id) &
            (api_data['value'] == current)
        ]['selection_id'].item()
        self.parent.parent.custom_mapping.update({self.name: match})
        self.parent.parent.build_selection_view_frame()


class MissingIdentificationFrame(CustomFrame):
    def __init__(self, parent: Interface) -> None:
        CustomFrame.__init__(self, parent, 'Missing Amazon Identification')
        self.parent = parent
        self.variations: DataFrame
        self.identification_view_frame: IdentificationViewFrame = None
        self.ignored_identifications: List[str] = []
        self.custom_mapping: Dict[str, Dict[str, str]] = defaultdict(dict)
        self.refresh_identification = tkinter.Button(
            self, text='Refresh ASIN & FNSKU',
            command=self.__refresh_identification,
            background=ALTERNATIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR,
            activebackground=ALTERNATIVE_ACTIVE_BUTTON_COLOR,
            activeforeground=ALTERNATIVE_TEXT_COLOR
        )
        self.ignore_all_button = tkinter.Button(
            self, text='Use all unmodified', command=self.__ignore_all_variations,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.ignore_selection_button = tkinter.Button(
            self, text='Use selection unmodified',
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR,
            command=self.__ignore_selected_variations
        )
        self.next_button = tkinter.Button(
            self, text='Next', command=partial(
                parent.show_frame, 'ITEM_DATA'
            ), background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.next_button['state'] = 'disabled'
        self.back_button = tkinter.Button(
            self, text='Back', command=partial(
                parent.show_frame, 'FLATFILE'
            ),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.blocked_notification = tkinter.Label(
            self, text='blocked until the list is empty',
            font=SMALL_NOTE_FONT, background=FRAME_COLOR,
            foreground=TEXT_COLOR
        )
        self.refresh_identification.place(
            x=25, y=TOP_SEPARATOR_Y + 10, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT / 2
        )
        tkinter.ttk.Separator(
            self, orient='horizontal'
        ).place(
            x=0, y=TOP_SEPARATOR_Y + 15 + BUTTON_HEIGHT / 2,
            width=WINDOW_WIDTH, height=2
        )
        self.ignore_all_button.place(
            x=NEXT_BUTTON_X - 20, y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10,
            width=BUTTON_WIDTH, height=BUTTON_HEIGHT
        )
        self.ignore_selection_button.place(
            x=NEXT_BUTTON_X - 20 + BUTTON_WIDTH,
            y=BOTTOM_SEPARATOR_Y - BUTTON_HEIGHT - 10, width=BUTTON_WIDTH + 30,
            height=BUTTON_HEIGHT
        )
        self.next_button.place(
            x=NEXT_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.blocked_notification.place(
            x=NEXT_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y + BUTTON_HEIGHT + 5,
            width=BUTTON_WIDTH, height=10
        )
        self.back_button.place(
            x=BACK_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )

    def build_variation_view_frame(self) -> None:
        """
        To ensure that we first fetch the attributes before building the frame
        """
        if self.identification_view_frame:
            self.identification_view_frame.place_forget()
        height = BOTTOM_SEPARATOR_Y - 1.5 * BUTTON_HEIGHT - 30 -\
            TOP_SEPARATOR_Y
        self.identification_view_frame = IdentificationViewFrame(parent=self)
        self.identification_view_frame.place(
            x=25, y=TOP_SEPARATOR_Y + BUTTON_HEIGHT / 2 + 20,
            width=WINDOW_WIDTH-60, height=height
        )
        if not self.identification_view_frame.identification_rows:
            self.blocked_notification.place_forget()
            self.next_button['state'] = 'normal'

    def __refresh_identification(self) -> None:
        self.parent.missing_amazon_identification_check(refresh=True)

    def __ignore_all_variations(self) -> None:
        self.ignored_identifications =\
            self.identification_view_frame.variations['sku'].to_list()
        self.build_variation_view_frame()

    def __ignore_selected_variations(self) -> None:
        ignored = []
        for row in self.identification_view_frame.identification_rows:
            if row.selected.get():
                ignored.append(row.name)
        self.ignored_identifications = ignored
        self.build_variation_view_frame()


class IdentificationViewFrame(ScrollableFrame):
    def __init__(self, parent: MissingIdentificationFrame) -> None:
        ScrollableFrame.__init__(self, parent)

        self.parent = parent
        self.variations = self.parent.variations
        self.identification_rows: List[IdentificationRow]
        self.populate()

    def populate(self):
        self.identification_rows = []
        tkinter.Label(
            self.frame, text='SKU', background=FRAME_COLOR,
            foreground=TEXT_COLOR, font=PROPERTY_HEADER_FONT
        ).grid(row=0, column=1)
        tkinter.Label(
            self.frame, text='ASIN', background=FRAME_COLOR,
            foreground=TEXT_COLOR, font=PROPERTY_HEADER_FONT
        ).grid(row=0, column=2)
        tkinter.Label(
            self.frame, text='FNSKU', background=FRAME_COLOR,
            foreground=TEXT_COLOR, font=PROPERTY_HEADER_FONT
        ).grid(row=0, column=3)
        for index, variation in enumerate(self.variations['sku']):
            if (
                variation in self.parent.ignored_identifications or
                variation in self.parent.custom_mapping
            ):
                continue
            codes = self.variations.loc[index, :][['asin', 'fnsku']].to_list()
            row = IdentificationRow(
                parent=self, name=variation, codes=codes, row_index=index + 1
            )
            self.identification_rows.append(row)


class IdentificationRow:
    def __init__(
        self, parent: IdentificationViewFrame, name: str, codes: List[str],
        row_index: int
    ) -> None:
        self.parent = parent
        self.row_index = row_index
        self.name = name
        self.codes = codes
        self.selected = tkinter.BooleanVar()
        self.checkbox = tkinter.Checkbutton(
            self.parent.frame, variable=self.selected,
            background=FRAME_COLOR, foreground=TEXT_COLOR,
            activebackground=ACTIVE_BUTTON_COLOR,
            highlightthickness=0
        )
        self.label = tkinter.Label(
            self.parent.frame, text=f'{name}', background=FRAME_COLOR,
            foreground=TEXT_COLOR, font=NORMAL_FONT
        )
        self.asin_value = tkinter.StringVar()
        self.asin_entry = tkinter.Entry(
            self.parent.frame, textvariable=self.asin_value,
            background=ENTRY_COLOR, foreground=TEXT_COLOR,
            width=12
        )
        self.asin_value.trace('w', self.__asin_check)
        self.asin_value.set(codes[0])
        self.fnsku_value = tkinter.StringVar()
        self.fnsku_entry = tkinter.Entry(
            self.parent.frame, textvariable=self.fnsku_value,
            background=ENTRY_COLOR, foreground=TEXT_COLOR,
            width=12
        )
        self.fnsku_value.trace('w', self.__fnsku_check)
        self.fnsku_value.set(codes[1])
        self.save_button = tkinter.Button(
            self.parent.frame, text='save changes',
            command=self.__save,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.checkbox.grid(row=row_index, column=0, padx=10)
        self.label.grid(row=row_index, column=1, padx=20)
        self.asin_entry.grid(row=row_index, column=2, padx=10)
        self.fnsku_entry.grid(row=row_index, column=3, padx=10)
        self.save_button.grid(row=row_index, column=4, padx=10)

    def __check(self, field: str) -> None:
        value = getattr(self, f'{field}_value').get()
        if len(value) != 10:
            getattr(self, f'{field}_entry').configure(
                {'background': ERROR_COLOR}
            )
        else:
            getattr(self, f'{field}_entry').configure(
                {'background': ENTRY_COLOR}
            )

    def __asin_check(self, *args) -> None:
        del args
        self.__check(field='asin')

    def __fnsku_check(self, *args) -> None:
        del args
        self.__check(field='fnsku')

    def __save(self, *args) -> None:
        del args
        # Get the flatfile as pandas DataFrame from the Interface class
        asin_value = self.asin_value.get()
        fnsku_value = self.fnsku_value.get()
        self.parent.parent.custom_mapping.update(
            {self.name: {'asin': asin_value, 'fnsku': fnsku_value}}
        )
        self.parent.parent.build_variation_view_frame()


class ExecutionFrame(CustomFrame):
    def __init__(self, parent) -> None:
        CustomFrame.__init__(self, parent, 'Build upload files')
        self.parent = parent
        self.status_text = tkinter.StringVar()
        self.status = tkinter.Label(
            self, textvariable=self.status_text, background=FRAME_COLOR,
            foreground=TEXT_COLOR, font=STATUS_FONT
        )
        self.back_button = tkinter.Button(
            self, text='Restart', command=partial(
                parent.show_frame, 'FLATFILE'
            ),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.back_button['state'] = 'disabled'
        self.exit_button = tkinter.Button(
            self, text='Exit', command=parent.root.destroy,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.status.place(
            x=50, y=WINDOW_HEIGHT / 2 - 50, width=WINDOW_WIDTH - 100,
            height=100
        )
        self.back_button.place(
            x=NEXT_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.exit_button.place(
            x=BACK_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )

    def execute(self, actions: List[Command]) -> None:
        for action in actions:
            self.status_text.set(action['title'])
            self.parent.root.update()
            return_value = action['command']()
            if not return_value:
                self.status_text.set(f"failed at \"{action['title']}\"")
                self.back_button['state'] = 'normal'
                return
        self.status_text.set('complete')
        self.back_button['state'] = 'normal'


class ItemDataFrame(CustomFrame):
    def __init__(self, parent) -> None:
        CustomFrame.__init__(self, parent, 'Customize the item')
        self.parent = parent
        self.parent = parent
        self.category_label = tkinter.Label(
            self, text='Choose the categories for the product',
            background=FRAME_COLOR, foreground=TEXT_COLOR, font=NORMAL_FONT
        )
        self.category_options = self.parent.create_mapping(section='CATEGORY')
        self.option_value = tkinter.StringVar()
        self.option_menu = tkinter.ttk.Combobox(
            self, textvariable=self.option_value,
            exportselection=False, takefocus=False,
            values=list(self.category_options), state='readonly'
        )
        self.option_menu.set(list(self.category_options)[0])
        self.current_category_choices = tkinter.StringVar()
        self.categy_choices = tkinter.Entry(
            self, textvariable=self.current_category_choices,
            background=ENTRY_COLOR, foreground=TEXT_COLOR
        )
        if self.parent.args.categories:
            self.current_category_choices.set(self.parent.args.categories)
        self.option_value.trace(
            'w', self.__adjust_current_category_choices
        )
        self.first_name = LabeledEntry(
            parent=self,
            label_text='Choose a name for the product (Name 1 - intern)',
            label_len=350, entry_len=250
        )
        if self.parent.args.name1:
            self.first_name.entry_text.set(self.parent.args.name1)
        self.third_name = LabeledEntry(
            parent=self,
            label_text='Choose the display name for the webshop (Name 3)',
            label_len=350, entry_len=250
        )
        if self.parent.args.name3:
            self.third_name.entry_text.set(self.parent.args.name3)
        self.state = {
            'name': tkinter.BooleanVar(),
            'categories': tkinter.BooleanVar(),
            'webshopname': tkinter.BooleanVar()
        }
        self.current_category_choices.trace('w', self.__check_state)
        self.first_name.entry_text.trace('w', self.__check_state)
        self.third_name.entry_text.trace('w', self.__check_state)
        self.next_button = tkinter.Button(
            self, text='Next', command=parent.create_files,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.next_button['state'] = 'disabled'
        self.__check_state()
        self.back_button = tkinter.Button(
            self, text='Back', command=partial(
                parent.show_frame, 'FLATFILE'
            ),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.category_label.place(x=100, y=100, width=500, height=20)
        self.option_menu.place(x=100, y=120, width=500, height=30)
        self.categy_choices.place(x=100, y=170, width=500, height=40)
        self.first_name.place(x=50, y=250, width=600, height=50)
        self.third_name.place(x=50, y=330, width=600, height=50)
        self.next_button.place(
            x=NEXT_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.back_button.place(
            x=BACK_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )

    def get(self) -> CustomData:
        return {
            'name1': self.first_name.get(),
            'categories': self.current_category_choices.get().split(','),
            'name3': self.third_name.get()
        }

    def __check_category_input(self) -> bool:
        categories = self.current_category_choices.get()
        if not categories:
            return False
        if categories.find(','):
            category_ids = list(categories.split(','))
        else:
            category_ids = [categories]
        for category in category_ids:
            try:
                int(category)
            except ValueError:
                return False
        return True

    def __check_name_input(self, name: str) -> bool:
        if not name:
            return False
        if len(name) < 3:
            return False
        return True

    def __check_state(self, *args) -> None:
        del args
        self.state['categories'].set(self.__check_category_input())
        for fields in [
            (self.first_name.entry_text.get(), 'name'),
            (self.third_name.entry_text.get(), 'webshopname')
        ]:
            self.state[fields[1]].set(self.__check_name_input(name=fields[0]))
        if all(x.get() for x in self.state.values()):
            self.next_button['state'] = 'normal'
        else:
            self.next_button['state'] = 'disabled'


    def __cleanup_category_input(self) -> None:
        current = self.current_category_choices.get()
        if not current:
            return
        validated = ''
        for index, part in enumerate(current.replace(' ', '').split(',')):
            try:
                part_num = int(part)
            except ValueError:
                continue
            if index < len(current) - 1:
                validated += f'{part_num},'
            else:
                validated += str(part_num)
        self.current_category_choices.set(validated)

    def __adjust_current_category_choices(self, *args) -> None:
        del args
        category_id = self.category_options[
            self.option_value.get()
        ]
        current = self.current_category_choices.get()
        if current:
            self.__cleanup_category_input()
            current += f',{category_id}'
        else:
            current = str(category_id)
        self.current_category_choices.set(current)


class FlatfileChooserFrame(CustomFrame):
    def __init__(self, parent) -> None:
        CustomFrame.__init__(self, parent, 'Choose a Flatfile')
        self.parent = parent
        self.input_folder = parent.folders['input']
        self.flatfile = tkinter.StringVar()
        self.flatfile.trace('w', self.__read_flatfile)
        self.dataframe = DataFrame()
        self.chooser_button = tkinter.Button(
            self, text='Select file', command=self.choose_flatfile,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.choice_description = tkinter.Label(
            self, text='Current selection:',
            background=FRAME_COLOR, foreground=TEXT_COLOR,
            font=NORMAL_FONT
        )
        self.current_choice = tkinter.Label(
            self, textvariable=self.flatfile,
            background=FRAME_COLOR, foreground=TEXT_COLOR,
            font=HIGHLIGHT_FONT
        )
        self.next_button = tkinter.Button(
            self, text='Next', command=parent.missing_color_check,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.next_button['state'] = 'disabled'
        self.exit_button = tkinter.Button(
            self, text='Exit', command=parent.root.destroy,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR
        )
        self.chooser_button.place(
            x=WINDOW_WIDTH / 4, y=WINDOW_HEIGHT / 4,
            width=WINDOW_WIDTH / 2, height=WINDOW_HEIGHT / 4
        )
        self.next_button.place(
            x=NEXT_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT
        )
        self.exit_button.place(
            x=BACK_BUTTON_X, y=BOTTOM_ROW_BUTTON_Y, width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT)

    def choose_flatfile(self) -> None:
        self.flatfile.set(
            tkinter.filedialog.askopenfilename(
                initialdir=self.input_folder,
                title='Amazon flatfile as `.csv`',
                filetypes=[("csv", "*.csv")]
            )
        )
        self.choice_description.place(
            x=WINDOW_WIDTH / 10, y=WINDOW_HEIGHT / 2 + (WINDOW_HEIGHT / 8),
            width=140, height=WINDOW_HEIGHT / 16)
        self.current_choice.place(
            x=WINDOW_WIDTH / 10,
            y=WINDOW_HEIGHT / 2 + (WINDOW_HEIGHT / 8) + (WINDOW_HEIGHT / 16),
            width=round(WINDOW_WIDTH / 1.2), height=WINDOW_HEIGHT / 16
        )

    def __read_flatfile(self, *args) -> None:
        del args
        self.parent.read_flatfile(flatfile=self.flatfile.get())
        if len(self.parent.dataframe.index) > 0:
            self.next_button['state'] = 'normal'
