"""
Plentymarkets Item upload
Copyright (C) 2022 Sebastian Fricke Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import configparser
from pathlib import Path
import platformdirs

from item_upload.interface import Interface


def setup_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--custom_config_dir', '--config', required=False,
                        help='Provide a custom path to a directory containing '
                        'the config.ini configuration file',
                        dest='custom_config', type=Path)
    parser.add_argument('--custom_input_dir', '--input', required=False,
                        help='Provide a custom path to a directory containing '
                        'the input flatfiles',
                        dest='custom_input', type=Path)
    parser.add_argument('--custom_output_dir', '--output', required=False,
                        help='Provide a custom path to a directory for storing'
                        ' the resulting upload files for plentymarkets',
                        dest='custom_output', type=Path)
    parser.add_argument('--name1', '--n1', required=False,
                        help='ItemTextName field number 1 in Plentymarkets',
                        dest='name1')
    parser.add_argument('--name3', '--n3', required=False,
                        help='ItemTextName field number 3 in Plentymarkets',
                        dest='name3')
    parser.add_argument('--Categories', '-c', required=False,
                        help='Comma-separated list of Plentymarkets category '
                        'IDs, the first ID is assigned as standard category.',
                        dest='categories')
    parser.add_argument('--flatfile', '-f', required=False,
                        help='Filepath to the Amazon Flatfile.',
                        dest='flatfile', type=Path)
    return parser.parse_args()


def main():
    args = setup_argparser()
    if args.custom_config:
        config_path = args.custom_config / 'config.ini'
    else:
        config_path = Path(
            platformdirs.user_config_dir('item_upload')
        ) / 'config.ini'
    config_path.parent.mkdir(exist_ok=True, parents=True)
    config = configparser.ConfigParser()
    config.read(config_path)

    if args.custom_input:
        input_folder = args.custom_input
    else:
        input_folder = Path(platformdirs.user_documents_dir()) / 'item_upload'
    input_folder.mkdir(parents=True, exist_ok=True)

    if args.custom_output:
        upload_folder = args.custom_output
    else:
        upload_folder = Path(
            platformdirs.user_documents_dir()
        ) / 'item_upload' / 'output'
    upload_folder.mkdir(parents=True, exist_ok=True)

    interface = Interface(
        config=config, args=args,
        folders={'input': input_folder, 'output': upload_folder}
    )
    interface.root.mainloop()
