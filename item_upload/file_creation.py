"""
Plentymarkets Item upload
Copyright (C) 2022 Sebastian Fricke Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from configparser import ConfigParser
from datetime import date
from pathlib import Path
from string import Template
import re
from typing import Dict, List, Union, TypedDict
from tkinter import messagebox
import numpy as np
import pandas

from loguru import logger
from item_upload.plentymarkets_connector import (
    AttributeService, BarcodeService
)
from item_upload.flatfile_handling import (
    TYPE_FIELD, SKU_FIELD, EAN_FIELD, NAME_FIELD, PRICE_FIELD, COLOR_FIELD,
    SIZE_FIELD, PSKU_FIELD, RELATION_FIELD, DESC_FIELD, KEYWORD_FIELD,
    LENGTH_FIELD, WIDTH_FIELD, HEIGHT_FIELD, WEIGHT_FIELD, ITEM_WEIGHT_FIELD
)


# Fixed constants
ACCOUNT_ID = 0
MARKET_ID = 104



class CustomData(TypedDict):
    name1: str
    categories: List[str]
    name3: str


class WrongEncodingException(Exception):
    pass


class FileCreationService:
    def __init__(
        self, config: ConfigParser, flatfile: pandas.DataFrame,
        upload_folder: Path
    ) -> None:
        self.config = config
        self.flatfile = flatfile
        self.upload_folder = upload_folder
        self.attribute_service = AttributeService(config=config)
        self.barcode_service = BarcodeService(config=config)
        self.barcode_service.pull_barcodes()
        self.file_ending = ''

    def set_file_ending(self, name: str) -> None:
        specific_name = re.sub(r'[(){}<>\\/"\'\\ ]', '', name)
        self.file_ending = f'{specific_name}_{date.today()}.csv'

    def item_upload(self, input_data: CustomData) -> bool:
        column_names = [
            'Parent-SKU', 'SKU',
            'is_parent',
            'Length', 'Width',
            'Height', 'Weight', 'Item-Weight',
            'Name', 'Attributes', 'Position',
            'ItemTextKeywords',
            'ItemTextName', 'ItemTextName3', 'ItemTextDescription',
            'Category-IDs',
            'Standard-Category', 'Standard-Category-Webshop',
            'EAN_Barcode', 'FNSKU_Barcode',
            'marketid', 'accountid',
            'amazon_sku', 'amazon_parentsku',
            'amazon-producttype',
            'price', 'ASIN-countrycode', 'ASIN-type', 'ASIN-value'
        ]
        standard_cat = input_data['categories'][0]

        df = self.flatfile
        result_df = pandas.DataFrame(columns=column_names)

        target_keys = [
            'SKU', 'Parent-SKU', 'Length', 'Width', 'Height',
            'Weight', 'Item-Weight', 'Name', 'ItemTextDescription', 'EAN_Barcode',
            'ItemTextKeywords', 'amazon_sku', 'amazon_parentsku', 'price'
        ]
        source_keys = [
            SKU_FIELD, PSKU_FIELD, LENGTH_FIELD, WIDTH_FIELD, HEIGHT_FIELD,
            WEIGHT_FIELD, ITEM_WEIGHT_FIELD, NAME_FIELD, DESC_FIELD, EAN_FIELD,
            KEYWORD_FIELD, SKU_FIELD, PSKU_FIELD, PRICE_FIELD
        ]
        key_combi = zip(source_keys, target_keys)
        for s_key, t_key in key_combi:
            result_df[t_key] = df[s_key]

        result_df['is_parent'] = df[RELATION_FIELD].apply(
            lambda x: not x == 'child')

        # TODO this is probably quite inefficient
        result_df.index = result_df['SKU']
        result_df['Attributes'] = self.__add_attribute_string()
        result_df.reset_index(inplace=True, drop=True)
        missing_keywords = np.where(result_df['ItemTextKeywords'] == '',
                                    result_df['SKU'], 'N')
        missing_keywords = missing_keywords[~(missing_keywords == 'N')]
        if missing_keywords.any():
            logger.warning(
                f"The following SKUs do not have keywords:\n{missing_keywords}"
            )

        result_df['ItemTextName'] = input_data['name1']
        result_df['ItemTextName3'] = input_data['name3']
        result_df['Category-IDs'] = ','.join(input_data['categories'])
        result_df['Standard-Category'] = standard_cat
        result_df['Standard-Category-Webshop'] = standard_cat
        result_df['amazon-producttype'] = df[TYPE_FIELD].apply(
            lambda x: get_producttype_id(product_type=x)
        )
        child_index = result_df[~result_df['is_parent']].index
        result_df.loc[child_index, 'ASIN-countrycode'] = '0'
        result_df.loc[child_index, 'ASIN-type'] = 'ASIN'
        result_df['ASIN-value'] = result_df['SKU'].apply(
            lambda x: self.barcode_service.get_asin(sku=x)
        )
        result_df['FNSKU_Barcode'] = result_df['SKU'].apply(
            lambda x: self.barcode_service.get_fnsku(sku=x)
        )
        result_df['marketid'] = MARKET_ID
        result_df['accountid'] = ACCOUNT_ID

        # Assign prices to the parent rows
        new_index = []
        position = []
        parent = df[df[RELATION_FIELD] == 'parent']
        for parent_entry in parent.itertuples():
            parent_sku = getattr(parent_entry, SKU_FIELD)
            childs = (df[PSKU_FIELD] == parent_sku) &\
                (df[RELATION_FIELD] != 'parent')
            any_price = df[childs][PRICE_FIELD].max()
            result_df.loc[parent_entry.Index, 'price'] = any_price
            # Adjust the postions
            new_index.append(parent_entry.Index)
            new_index += df[childs].index.tolist()
            position.append(0)
            position += [i+1 for i, _ in enumerate(df[childs].index.tolist())]

        result_df = result_df.reindex(new_index)
        result_df.reset_index(inplace=True, drop=True)
        result_df['Position'] = position

        filename = self.upload_folder / f"item_upload_{self.file_ending}"
        self.__write_csv_with_permission_check(result_df, filename)
        return True

    def __add_attribute_string(self) -> pandas.Series:
        """
        Build a valid attribute string for the Plentymarkets import file.

        This function is currently specialized for the Variation Themes:
            Size, Color and SizeColor
        If you need other attributes, then you first have to generalize this
        function.

        Returns:
                                [Series]            -   Pandas series with the
                                                        attribute string as
                                                        value and the SKU as
                                                        index
        """
        attribute_map = self.attribute_service.pull_attribute_names()
        result_data = {}
        for parent, children in self.flatfile.groupby(PSKU_FIELD):
            if not parent:
                continue
            unique_colors = children[COLOR_FIELD].unique()
            unique_sizes = children[SIZE_FIELD].unique()
            if len(unique_colors) <= 1 and len(unique_sizes) > 1:
                template = Template(f"{attribute_map['size']}:${SIZE_FIELD}")
            elif len(unique_colors) > 1 and len(unique_sizes) <= 1:
                template = Template(f"{attribute_map['color']}:${COLOR_FIELD}")
            else:
                template = Template(
                    f"{attribute_map['color']}:${COLOR_FIELD};"
                    f"{attribute_map['size']}:${SIZE_FIELD}")
            result_data.update(
                {parent: ''}
            )
            for child in children.to_dict(orient='records'):
                result_data.update(
                    {child['item_sku']: template.substitute(**child)}
                )
        return pandas.Series(result_data)

    @staticmethod
    def __write_csv_with_permission_check(dataframe, filepath) -> None:
        """
        This file is trying to solve a PermissionError on Windows while
        accessing open files.
        To not run into this problem, we check if there is any process
        accessing the file, if there is we give the user an error message,
        asking him the to close the process. Then the function starts checking
        again if there is still another process accessing the file.

        Parameters:
            dataframe           [dataframe]     -   the dataframe we got from
                                                    pandas to edit a file
            filepath            [string]        -   destination path of the
                                                    file to be created
        """
        while 1:
            try:
                dataframe.to_csv(filepath, sep=';', index=False)
                logger.info(
                    f'Upload file successfully created under {filepath}.'
                )
                break
            except PermissionError:
                messagebox.showerror(
                    'File is still open',
                    f'Please close the file {filepath} before continuing!'
                )
                continue

    def create_property_upload_file(
        self, configuration_mapping: Dict[str, int]
    ) -> bool:
        dataframe_rows = []
        if not configuration_mapping:
            logger.info(
                'Skip Property upload file creation, configuration section '
                '`PROPERTY` empty or missing.'
            )
            return True
        for count, sku in enumerate(self.flatfile['item_sku']):
            for key in configuration_mapping:
                if key in self.flatfile.columns:
                    column_value = self.flatfile.loc[count, key]
                    dataframe_rows.append(
                        [
                            sku, configuration_mapping[key], column_value,
                            'DE', 1
                        ]
                    )
        columns = ['SKU', 'ID-property', 'Value', 'Lang', 'Active']
        filepath = self.upload_folder / f'property_{self.file_ending}'
        self.__write_csv_with_permission_check(
            dataframe=pandas.DataFrame(dataframe_rows, columns=columns),
            filepath=filepath
        )
        return True

    def create_feature_upload_file(
        self, selection_mapping: Dict[str, Dict[str, int]],
        configuration_mapping: Dict[str, Union[Dict[str, int], None]]
    ) -> bool:
        invalid_columns = []
        dataframe_rows = []
        if not configuration_mapping:
            logger.info(
                'Skip Feature upload file creation, configuration sections '
                '`FEATURE` and `SELECTION` empty or missing.'
            )
            return True
        for count, sku in enumerate(self.flatfile['item_sku']):
            for value_type, values in configuration_mapping.items():
                if not values:
                    continue
                for flatfile_column in values:
                    # Skip columns directly if they have not been found
                    # in the flatfile previously
                    if flatfile_column in invalid_columns:
                        continue
                    property_id = values[flatfile_column]
                    try:
                        column_value = self.flatfile.loc[
                            count, flatfile_column
                        ]
                    except KeyError:
                        logger.warning(
                            f"Column {flatfile_column} not found in the "
                            "flatfile."
                        )
                        invalid_columns.append(flatfile_column)
                        continue
                    if not column_value:
                        continue

                    if value_type == 'text':
                        dataframe_rows.append([sku, property_id, column_value])
                        continue

                    if value_type == 'selection':
                        try:
                            selection_id = selection_mapping[
                                str(property_id)
                            ][column_value]
                        except KeyError:
                            continue
                        dataframe_rows.append(
                            [sku, property_id, selection_id]
                        )

        columns = [
            'Variation.number', 'VariationEigenschaften.id',
            'VariationEigenschaften.value'
        ]

        filepath = self.upload_folder / f'feature_{self.file_ending}'
        self.__write_csv_with_permission_check(
            dataframe=pandas.DataFrame(dataframe_rows, columns=columns),
            filepath=filepath
        )
        return True


def get_producttype_id(product_type: str) -> str:
    """
        Parameter:
            product_type    [str]   -   product type value for a given row

        Description:
            Search for a matching term in mapping list of valid
            amazon producttype names and their ID.

        Return:
            [str]
            value from type_id on success
            empty string on failure
    """
    type_id = {
        'accessory': '28',
        'shirt': '13',
        'pants': '15',
        'dress': '18',
        'outerwear': '21',
        'bag': '27',
        'furnitureanddecor': '4',
        'bedandbath': '3',
        'skirt': '123',
        'swimwear': '30',
        'exercisemat': '1199'
    }

    if not product_type:
        return ''

    if product_type.lower() in type_id.keys():
        return type_id[product_type.lower()]
    return ''


def get_standard_category(cat):
    """
        Parameter:
            cat [String] => String representation of category ids
                            separated by ','

        Description:
            Get the first entry from a list of category ids, with a maximum
            length of 3 digits.

        Return:
            [INT] integer representation of the category ID
    """
    if not cat:
        return 0
    value = cat[0:3].strip(',')
    try:
        cat_id = int(value)
    except ValueError:
        return 0

    return cat_id
