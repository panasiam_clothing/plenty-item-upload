"""
Plentymarkets Item upload
Copyright (C) 2022 Sebastian Fricke Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from collections import defaultdict
from configparser import ConfigParser
from datetime import date, timedelta
import re
import pathlib
from tkinter import messagebox
from typing import Dict, List
import numpy as np
import pandas
import platformdirs
from plenty_api import PlentyApi

from loguru import logger

from item_upload.flatfile_handling import (
    RELATION_FIELD, SKU_FIELD, COLOR_FIELD
)


class CacheHandler:
    """ Service to cache the attributes from the REST API """
    def __init__(self):
        self.destination = pathlib.Path(
            platformdirs.user_data_dir(appname='item_upload')
        )
        self.destination.mkdir(parents=True, exist_ok=True)

    def cache_data(self, dataframe: pandas.DataFrame, name: str) -> None:
        """ Serialize and cache data """
        full_path = self.destination / f'{name}.pkl'
        dataframe.to_pickle(full_path)

    def read_data(self, name: str) -> pandas.DataFrame:
        """ Deserialize and read data """
        today = date.today()
        valid_date = today - timedelta(days=7)
        full_path = self.destination / f'{name}.pkl'
        if not full_path.exists():
            return pandas.DataFrame()
        if date.fromtimestamp(full_path.stat().st_mtime) < valid_date:
            try:
                full_path.unlink()
            except PermissionError:
                logger.error(
                    f'Unable to delete old cache file at {full_path} due to '
                    'insufficient permissions.'
                )
            return pandas.DataFrame()
        return pandas.read_pickle(full_path)

    def remove_data(self, name: str) -> None:
        full_path = self.destination / f'{name}.pkl'
        full_path.unlink()


class SelectionFeatureService:
    """
    Handle download and upload of selection values from features in
    Plentymarkets.
    """
    def __init__(self, config: ConfigParser, api_instance: PlentyApi = None):
        self.config = config
        self.names: pandas.DataFrame
        self.selections: pandas.DataFrame
        try:
            assert config.has_section(section='SELECTION')
            assert config.has_section(section='PLENTY')
            assert config.has_option(section='PLENTY',
                                     option='selection_value_lang')
        except AssertionError as err:
            raise RuntimeError(
                'To pull selections from the Plentymarkets API, a SELECTION '
                'section is required within the configuration and the PLENTY '
                'section must contain the entries: `base_url`, '
                '`selection_value_lang`'
            ) from err
        if not api_instance:
            self.api = PlentyApi(base_url=config['PLENTY']['base_url'])
        else:
            self.api = api_instance
        self.cache = CacheHandler()

    def create_selections(self, selections: Dict[int, List[str]]) -> bool:
        message_string = '; '.join(
            [
                f"({key}: {', '.join(value)})"
                for key, value in selections.items()
            ]
        )
        if not messagebox.askokcancel(
            title='Create selection values on Plentymarkets',
            message='Please confirm that you want to create the following '
            f'selection values.\n{message_string}'
        ):
            return False
        for property_id, values in selections.items():
            for selection_value in values:
                self.__create_new_selection(
                    property_id=property_id, name=selection_value
                )
        return True

    def __create_new_selection(self, property_id: int, name: str) -> None:
        self.api.plenty_api_create_property_selection(
            property_id=property_id, position=0, names=[
                {
                    'lang': self.config.get(
                        section='PLENTY', option='selection_value_lang'
                    ),
                    'name': name
                }
            ]
        )

    def pull_property_names(
        self, overwrite_cache: bool = False
    ) -> pandas.DataFrame:
        names = pandas.DataFrame()
        if not overwrite_cache:
            names = self.cache.read_data(name='property_names')
        if len(names.index) == 0:
            current_data_format = self.api.data_format
            self.api.data_format = 'dataframe'
            selection_lang = self.config.get(
                section='PLENTY', option='selection_value_lang'
            )
            names = self.api.plenty_api_get_property_names(
                lang=selection_lang
            )
            self.api.data_format = current_data_format
            if isinstance(names, type(None)):
                raise RuntimeError(
                    'Plentymarkets property names REST API request failed'
                )
            if isinstance(names, pandas.DataFrame) and len(names.index) == 0:
                raise RuntimeError(
                    'No properties found in Plentymarkets'
                )
            self.cache.cache_data(dataframe=names, name='property_names')
        self.names = names
        return names

    def pull_selections(
        self, overwrite_cache: bool = False
    ) -> pandas.DataFrame:
        dataframe = pandas.DataFrame()
        required_selections = [
            int(selection_id)
            for selection_id in self.config['SELECTION'].values()
        ]
        if not overwrite_cache:
            dataframe = self.cache.read_data(name='selections')
            if len(dataframe.index) > 0:
                if any(
                    actual not in required_selections
                    for actual in dataframe['property_id'].to_list()
                ):
                    # Overwrite the cached data as the configuration has
                    # been changed
                    dataframe = pandas.DataFrame()
        if len(dataframe.index) == 0:
            selections = self.api.plenty_api_get_property_selections()
            if not selections:
                raise RuntimeError(
                    'Plentymarkets Selection REST API request failed'
                )
            selection_lang = self.config.get(
                section='PLENTY', option='selection_value_lang'
            )
            selection_data = []
            for property_id, selections in selections.items():
                if property_id not in required_selections:
                    continue
                for selection_id, values in selections.items():
                    for lang, value in values.items():
                        if lang.lower() == selection_lang.lower():
                            selection_data.append(
                                [property_id, selection_id, value]
                            )
            dataframe = pandas.DataFrame(
                selection_data,
                columns=['property_id', 'selection_id', 'value']
            )
            self.cache.cache_data(dataframe=dataframe, name='selections')
        self.selections = dataframe
        return dataframe

    def generate_missing_dictionary(
        self, flatfile: pandas.DataFrame
    ) -> Dict[str, List[str]]:
        missing_selection_values = defaultdict(list)
        for column, property_id in self.config['SELECTION'].items():
            property_id = int(property_id)
            if column not in flatfile.columns:
                continue
            flatfile_values = flatfile[column].to_list()
            existing_values = self.selections[
                self.selections['property_id'] == property_id
            ]['value'].to_list()
            for value in set(flatfile_values):
                if not value:
                    continue
                if value not in existing_values:
                    missing_selection_values[property_id].append(value)
        return dict(missing_selection_values)

    def create_selection_mapping(
        self, flatfile: pandas.DataFrame, user_mapping: Dict[str, int] = None
    ) -> Dict[str, Dict[str, int]]:
        mapping = defaultdict(dict)
        for column, property_id in self.config['SELECTION'].items():
            if column not in flatfile.columns:
                continue
            flatfile_values = flatfile[column].to_list()
            matching_selections = self.selections[
                self.selections['property_id'] == int(property_id)
            ]
            for value in set(flatfile_values):
                if not value:
                    continue
                if value not in matching_selections['value'].to_list():
                    if user_mapping and value in user_mapping:
                        mapping.update({value: user_mapping[value]})
                        continue
                    logger.warning(
                        f'No mapped selection value found for value [{value}] '
                        f'in column {column}'
                    )
                    mapping.update({value: -1})
                    continue
                try:
                    selection_id = matching_selections[
                        matching_selections['value'] == value
                    ]['selection_id'].to_list()[0]
                except IndexError:
                    selection_id = -1
                mapping[str(property_id)].update({value: selection_id})
        return dict(mapping)


class BarcodeService:
    """ Handle download and upload of Amazon barcodes with Plentymarkets """
    def __init__(self, config: ConfigParser, api_instance: PlentyApi = None):
        self.config = config
        self.dataframe: pandas.DataFrame
        self.custom_mapping: Dict[str, Dict[str, str]] = {}
        self.refreshed_during_session = False
        try:
            assert config.has_section(section='PLENTY')
            asin_id = int(
                config.get(section='PLENTY', option='asin_country_id')
            )
            fnsku_id = int(
                config.get(section='PLENTY', option='fnsku_barcode_id')
            )
            self.configuration_values = {
                'asin_id': asin_id, 'fnsku_id': fnsku_id
            }
        except ValueError:
            pass
        except AssertionError:
            pass
        if not api_instance:
            self.api = PlentyApi(base_url=config['PLENTY']['base_url'])
        else:
            self.api = api_instance
        self.cache = CacheHandler()

    def pull_barcodes(self, overwrite_cache: bool = False) -> pandas.DataFrame:
        dataframe = pandas.DataFrame()
        if overwrite_cache:
            self.refreshed_during_session = True
        else:
            dataframe = self.cache.read_data(name='barcodes')
        if len(dataframe.index) == 0:
            variations = self.api.plenty_api_get_variations(
                additional=['variationBarcodes', 'marketItemNumbers']
            )
            if not variations:
                raise RuntimeError(
                    'Plentymarkets Variations REST API request failed'
                )
            dataframe_rows = []
            for variation in variations:
                try:
                    asin = [
                        market_number['value']
                        for market_number in variation['marketItemNumbers']
                        if market_number['type'] == 'ASIN' and
                        market_number['countryId'] ==
                        self.configuration_values['asin_id']
                    ][0]
                except IndexError:
                    asin = ''

                try:
                    fnsku = [
                        barcode['code']
                        for barcode in variation['variationBarcodes']
                        if barcode['barcodeId'] ==
                        self.configuration_values['fnsku_id']
                    ][0]
                except IndexError:
                    fnsku = ''
                sku = variation['number']
                dataframe_rows.append([sku, asin, fnsku])
            dataframe = pandas.DataFrame(
                dataframe_rows, columns=['sku', 'asin', 'fnsku']
            )
            self.cache.cache_data(dataframe=dataframe, name='barcodes')
        self.dataframe = dataframe
        return self.dataframe

    def get_data_for_flatfile(
        self, flatfile: pandas.DataFrame
    ) -> pandas.DataFrame:
        children = flatfile[flatfile[RELATION_FIELD] != 'parent'][SKU_FIELD]
        result = self.dataframe[
            self.dataframe['sku'].isin(children.to_list())
        ]
        children = children.rename('sku').to_frame()
        result = children.merge(
            result, on='sku', how='left'
        )
        result.fillna('', inplace=True)
        empty_fields_filter = (result['asin'] == '') | (result['fnsku'] == '')

        def sort_by_sku_string(
            dataframe: pandas.DataFrame
        ) -> pandas.DataFrame:
            numbers = dataframe.loc[dataframe['sku'].str.isdecimal()]
            numbers = numbers.astype({'sku': np.int64})
            numbers.sort_values('sku', inplace=True)
            numbers = numbers.astype({'sku': str})
            non_numbers = dataframe.loc[
                ~dataframe['sku'].str.isdecimal()
            ].copy()
            non_numbers.sort_values('sku', inplace=True)
            result = numbers.append(non_numbers, ignore_index=True)
            result.reset_index(inplace=True, drop=True)
            return result

        empty_values = sort_by_sku_string(
            dataframe=result.loc[empty_fields_filter]
        )
        filled_values = sort_by_sku_string(
            dataframe=result.loc[~empty_fields_filter]
        )
        result = empty_values.append(filled_values, ignore_index=True)
        result.reset_index(inplace=True, drop=True)
        return result

    def get_asin(self, sku: str) -> str:
        asin = ''
        if sku in self.custom_mapping and self.custom_mapping[sku]['asin']:
            return self.custom_mapping[sku]['asin']
        try:
            asin = self.dataframe.loc[
                self.dataframe['sku'] == sku
            ]['asin'].item()
        except ValueError:
            pass
        return asin

    def get_fnsku(self, sku: str) -> str:
        fnsku = ''
        if sku in self.custom_mapping and self.custom_mapping[sku]['fnsku']:
            return self.custom_mapping[sku]['fnsku']
        try:
            fnsku = self.dataframe.loc[
                self.dataframe['sku'] == sku
            ]['fnsku'].item()
        except ValueError:
            pass
        return fnsku


class AttributeService:
    """ Handle download and upload of attributes with Plentymarkets """
    def __init__(self, config: ConfigParser, api_instance: PlentyApi = None):
        self.config = config
        self.dataframe: pandas.DataFrame
        try:
            assert config.has_section(section='PLENTY')
            options = [
                'base_url', 'color_attribute_id', 'size_attribute_id',
                'color_attribute_lang'
            ]
            assert all(
                config.has_option(section='PLENTY', option=opt)
                for opt in options
            )
            self.configuration_values = {
                'color_id': int(config.get(
                    section='PLENTY', option='color_attribute_id'
                )),
                'size_id': int(config.get(
                    section='PLENTY', option='size_attribute_id'
                )),
                'lang': config.get(
                    section='PLENTY', option='color_attribute_lang'
                )
            }
            assert len(self.configuration_values['lang']) == 2
        except ValueError as err:
            raise RuntimeError(
                'The color and size attribute IDs must be integer values.'
            ) from err
        except AssertionError as err:
            raise RuntimeError(
                'Incomplete configuration, Plentymarkets REST API base URL '
                'and color & size attribute IDs and the desired color '
                'attribute value language are required.'
            ) from err
        if not api_instance:
            self.api = PlentyApi(base_url=config['PLENTY']['base_url'])
        else:
            self.api = api_instance
        self.cache = CacheHandler()

    def pull_attribute_names(self) -> Dict[str, str]:
        """
        Fetch a mapping of the backend name for the two required attributes.

        The backend name is required to set the attributes during the creation
        of new variations.
        """
        attributes = self.api.plenty_api_get_attributes()
        if not attributes:
            raise RuntimeError(
                'Plentymarkets Attribute REST API request failed'
            )
        color = self.configuration_values['color_id']
        size = self.configuration_values['size_id']
        required = [color, size]
        attribute_internal_name_map = {color: 'color', size: 'size'}
        attribute_names = {
            attribute_internal_name_map[attr['id']]: attr['backendName']
            for attr in attributes if attr['id'] in required
        }
        try:
            assert len(attribute_names) == 2
            return attribute_names
        except AssertionError as err:
            invalid_attribute = [
                name for name in ['color', 'size']
                if name not in attribute_names
            ]
            invalid_attribute_id = [
                key for key, name in attribute_internal_name_map.items()
                if name in invalid_attribute
            ]
            raise RuntimeError(
                'The following attribute IDs are not found in Plentymarkets: '
                f'{invalid_attribute_id}'
            ) from err

    def pull_color_attributes(
        self, overwrite_cache: bool = False
    ) -> pandas.DataFrame:
        dataframe = pandas.DataFrame()
        if not overwrite_cache:
            dataframe = self.cache.read_data(name='attributes')
        if len(dataframe.index) == 0:
            attributes = self.api.plenty_api_get_attributes(
                additional=['values']
            )
            if not attributes:
                raise RuntimeError(
                    'Plentymarkets Attribute REST API request failed'
                )
            color_attribute_id = self.configuration_values['color_id']
            lang = self.configuration_values['lang']
            attribute_data = []
            for attribute in attributes:
                if attribute['id'] != color_attribute_id:
                    continue
                for value in attribute['values']:
                    try:
                        color = [
                            (name['name'], value['id'])
                            for name in value['valueNames']
                            if name['lang'] == lang
                        ][0]
                    except IndexError:
                        logger.warning(
                            f"Color value {color}({value['id']}) doesn't have "
                            f"a name value for the {lang} language."
                        )
                        continue
                    attribute_data.append(
                        [
                            attribute['id'], value['backendName'],
                            value['position'], color[0], color[1]
                        ]
                    )
            dataframe = pandas.DataFrame(
                attribute_data,
                columns=[
                    'attributeId', 'backendName', 'position', 'valueName',
                    'valueId'
                ]
            )
            self.cache.cache_data(dataframe=dataframe, name='attributes')
        self.dataframe = dataframe
        return dataframe

    def __check_for_existing_data(self) -> None:
        try:
            assert len(self.dataframe.index) > 0
        except AssertionError as err:
            raise RuntimeError(
                '`pull_color_attributes` must be executed first.'
            ) from err

    def generate_missing_list(self, flatfile: pandas.DataFrame) -> List[str]:
        """
        Detect any color names that are not found on PlentyMarkets.

        Parameters:
            flatfile            [DataFrame]     -   Amazon Flatfile Dataframe

        Returns:
                                [list]          -   List of color names
        """
        self.__check_for_existing_data()
        assert COLOR_FIELD in flatfile.columns

        return sorted(list({
            color for color in flatfile[COLOR_FIELD].to_list()
            if color not in self.dataframe['valueName'].to_list()
        }))

    def create_color_suggestions(
        self, colors: List[str]
    ) -> Dict[str, List[str]]:
        self.__check_for_existing_data()
        suggestions = {}
        for color in colors:
            suggestions.update(
                {
                    color: self.__create_similar_color_name_suggestions(
                        color_name=color
                    )
                }
            )
        return suggestions

    def __create_similar_color_name_suggestions(self, color_name: str) -> list:
        colors = self.dataframe['valueName']
        matches = colors[
            # Search for exact matches with a maximum of 3 additional
            # characters in front and at the back
            # pylint: disable=consider-using-f-string
            colors.str.contains(
                r'^[\w\s]{0,3}%s[\w\s]{0,3}$' % color_name, regex=True,
                flags=re.IGNORECASE
            )
        ].to_list()
        matches = sorted(matches, key=len)
        for index, match in enumerate(matches):
            if match.lower() == color_name.lower():
                matches.insert(0, matches.pop(index))
        return matches

    def create_colors(self, colors: List[str]) -> bool:
        self.__check_for_existing_data()
        if not messagebox.askokcancel(
            title='Create attributes on Plentymarkets',
            message='Please confirm that you want to create the following '
            f"colors.\n{', '.join(colors)}"
        ):
            return False

        highest_position = int(self.dataframe['position'].max())
        color_attribute_id = self.configuration_values['color_id']
        lang = self.configuration_values['lang']

        json_data = []
        for index, color in enumerate(colors):
            json_data.append(
                {
                    'backendName': color,
                    'position': highest_position + index + 1
                }
            )

        response = self.api.plenty_api_create_attribute_values(
            attribute_id=color_attribute_id, json=json_data
        )
        if (
            (isinstance(response, dict) and 'error' in response) or
            (isinstance(response, list) and len(response) == 1 and
             'error' in response[0])
        ):
            error_descriptions = {
                'validation error found': 'Attribute already created'
            }
            if isinstance(response, list):
                response = response[0]
            message = response['error']['message']
            if message in error_descriptions:
                message = error_descriptions.get(message)
            raise RuntimeError(
                f'Attribute value creation request failed. ({message})'
            )
        for value in response:
            response = self.api.plenty_api_create_attribute_value_name(
                value_id=value['id'], lang=lang, name=value['backendName']
            )
            if isinstance(response, dict) and 'error' in response:
                raise RuntimeError(
                    'Attribute value name creation request failed.\n{response}'
                )
        return True
