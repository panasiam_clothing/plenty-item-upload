from configparser import ConfigParser
from pathlib import Path
import pytest
import pandas
from pandas.testing import assert_series_equal
import plenty_api

from item_upload.file_creation import FileCreationService
from item_upload.plentymarkets_connector import AttributeService


@pytest.fixture()
def fake_plenty(monkeypatch):
    # pylint: disable=invalid-name
    def fake_login(self, login_method: str, login_data: dict):
        del self, login_method, login_data
        return True
    monkeypatch.setattr(plenty_api.PlentyApi, '_PlentyApi__authenticate',
                        fake_login)


@pytest.fixture()
def fake_config():
    config = ConfigParser()
    config['PLENTY'] = {
        'base_url': 'abc.com', 'color_attribute_id': 1, 'size_attribute_id': 2,
        'color_attribute_lang': 'en', 'asin_country_id': 0,
        'fnsku_barcode_id': 3
    }
    return config


@pytest.fixture()
def fake_flatfile() -> pandas.DataFrame:
    return pandas.DataFrame()


@pytest.fixture()
def fake_file_creation_service(fake_config, fake_plenty, fake_flatfile):
    return FileCreationService(
        config=fake_config,
        flatfile=fake_flatfile,
        upload_folder=Path()
    )


@pytest.fixture()
def fake_pull_attributes(monkeypatch):
    # pylint: disable=invalid-name
    def fake_pull_attribute_names(self):
        del self
        return {'color': 'color_name', 'size': 'size_name'}
    monkeypatch.setattr(AttributeService, 'pull_attribute_names',
                        fake_pull_attribute_names)


def describe_add_attribute_string():
    def with_single_size_and_single_parent(
        fake_file_creation_service, fake_pull_attributes
    ) -> None:
        fs = fake_file_creation_service
        dataframe = pandas.DataFrame(
            [
                ['1230', '', '', ''],
                ['1234', 'blue', 'L', '1230'],
                ['1235', 'red', 'L', '1230'],
                ['1236', 'yellow', 'L', '1230']
            ], columns=['item_sku', 'color_name', 'size_name', 'parent_sku']
        )
        expectation = pandas.Series(
            ['', 'color_name:blue', 'color_name:red', 'color_name:yellow'],
            index=['1230', '1234', '1235', '1236']
        )
        fs.flatfile = dataframe
        assert_series_equal(
            fs._FileCreationService__add_attribute_string(), expectation
        )

    def with_single_size_and_two_parents(
        fake_file_creation_service, fake_pull_attributes
    ) -> None:
        fs = fake_file_creation_service
        dataframe = pandas.DataFrame(
            [
                ['1230', '', '', ''],
                ['1234', 'blue', 'L', '1230'],
                ['1235', 'red', 'L', '1230'],
                ['1236', 'yellow', 'L', '1230'],
                ['2340', '', '', ''],
                ['2345', 'blue', 'L', '2340'],
                ['2346', 'red', 'L', '2340']
            ], columns=['item_sku', 'color_name', 'size_name', 'parent_sku']
        )
        expectation = pandas.Series(
            [
                '', 'color_name:blue', 'color_name:red', 'color_name:yellow',
                '', 'color_name:blue', 'color_name:red'
            ],
            index=['1230', '1234', '1235', '1236', '2340', '2345', '2346']
        )
        fs.flatfile = dataframe
        assert_series_equal(
            fs._FileCreationService__add_attribute_string(), expectation
        )

    def with_multiple_sizes_and_single_parent(
        fake_file_creation_service, fake_pull_attributes
    ) -> None:
        fs = fake_file_creation_service
        dataframe = pandas.DataFrame(
            [
                ['1230', '', '', ''],
                ['1234', 'blue', 'L', '1230'],
                ['1235', 'blue', 'XL', '1230'],
                ['1236', 'red', 'L', '1230'],
                ['1237', 'red', 'XL', '1230'],
                ['1238', 'yellow', 'L', '1230']
            ], columns=['item_sku', 'color_name', 'size_name', 'parent_sku']
        )
        expectation = pandas.Series(
            [
                '', 'color_name:blue;size_name:L',
                'color_name:blue;size_name:XL', 'color_name:red;size_name:L',
                'color_name:red;size_name:XL','color_name:yellow;size_name:L'
            ],
            index=['1230', '1234', '1235', '1236', '1237', '1238']
        )
        fs.flatfile = dataframe
        assert_series_equal(
            fs._FileCreationService__add_attribute_string(), expectation
        )

    def with_multiple_sizes_and_multiple_parents(
        fake_file_creation_service, fake_pull_attributes
    ) -> None:
        fs = fake_file_creation_service
        dataframe = pandas.DataFrame(
            [
                ['1230', '', '', ''],
                ['1234', 'blue', 'L', '1230'],
                ['1235', 'blue', 'XL', '1230'],
                ['1236', 'red', 'L', '1230'],
                ['1237', 'red', 'XL', '1230'],
                ['1238', 'yellow', 'L', '1230'],
                ['2340', '', '', ''],
                ['2345', 'black', 'M', '2340'],
                ['2346', 'white', 'M', '2340'],
                ['2347', 'grey', 'M', '2340']
            ], columns=['item_sku', 'color_name', 'size_name', 'parent_sku']
        )
        expectation = pandas.Series(
            [
                '', 'color_name:blue;size_name:L',
                'color_name:blue;size_name:XL', 'color_name:red;size_name:L',
                'color_name:red;size_name:XL','color_name:yellow;size_name:L',
                '', 'color_name:black', 'color_name:white', 'color_name:grey'
            ],
            index=['1230', '1234', '1235', '1236', '1237', '1238', '2340',
                   '2345', '2346', '2347']
        )
        fs.flatfile = dataframe
        assert_series_equal(
            fs._FileCreationService__add_attribute_string(), expectation
        )
