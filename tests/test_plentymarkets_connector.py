from configparser import ConfigParser
from pandas._testing.asserters import assert_frame_equal
import pytest
import pandas
import plenty_api

from item_upload.plentymarkets_connector import (
    AttributeService, BarcodeService
)


@pytest.fixture()
def fake_plenty(monkeypatch):
    # pylint: disable=invalid-name
    def fake_login(self, login_method: str, login_data: dict):
        del self, login_method, login_data
        return True
    monkeypatch.setattr(plenty_api.PlentyApi, '_PlentyApi__authenticate',
                        fake_login)


@pytest.fixture()
def fake_config():
    config = ConfigParser()
    config['PLENTY'] = {
        'base_url': 'abc.com', 'color_attribute_id': 1, 'size_attribute_id': 2,
        'color_attribute_lang': 'en', 'asin_country_id': 0,
        'fnsku_barcode_id': 5
    }
    return config


@pytest.fixture()
def fake_attribute_service(fake_plenty, fake_config):
    return AttributeService(config=fake_config)


@pytest.fixture()
def fake_barcode_service(fake_plenty, fake_config):
    return BarcodeService(config=fake_config)


def describe_create_color_suggestions():
    def with_no_match(fake_attribute_service) -> None:
        attr_service = fake_attribute_service
        dataframe = pandas.DataFrame(
            [
                [1, 'blue_backend', 1, 'blue', 10],
                [1, 'black_backend', 2, 'black', 11]
            ],
            columns=[
                'attributeId', 'backendName', 'position', 'valueName',
                'valueId'
            ]
        )
        attr_service.dataframe = dataframe
        expectation = {'white': []}
        assert attr_service.create_color_suggestions(
            colors=['white']
        ) == expectation

    def with_one_match(fake_attribute_service) -> None:
        attr_service = fake_attribute_service
        dataframe = pandas.DataFrame(
            [
                [1, 'blue_backend', 1, 'blue', 10],
                [1, 'black_backend', 2, 'black', 11]
            ],
            columns=[
                'attributeId', 'backendName', 'position', 'valueName',
                'valueId'
            ]
        )
        attr_service.dataframe = dataframe
        expectation = {'Blue': ['blue']}
        assert attr_service.create_color_suggestions(
            colors=['Blue']
        ) == expectation

    def with_multiple_matches(fake_attribute_service) -> None:
        attr_service = fake_attribute_service
        dataframe = pandas.DataFrame(
            [
                [1, 'blue_backend', 1, 'blue', 10],
                [1, 'blueish_backend', 3, 'blueish', 12],
                [1, 'blue_2_backend', 4, '2Blue', 13],
                [1, 'black_backend', 2, 'black', 11]
            ],
            columns=[
                'attributeId', 'backendName', 'position', 'valueName',
                'valueId'
            ]
        )
        attr_service.dataframe = dataframe
        expectation = {'Blue': ['blue', '2Blue', 'blueish']}
        assert attr_service.create_color_suggestions(
            colors=['Blue']
        ) == expectation

    def with_multiple_colors(fake_attribute_service) -> None:
        attr_service = fake_attribute_service
        dataframe = pandas.DataFrame(
            [
                [1, 'blue_backend', 1, 'blue', 10],
                [1, 'red_backend', 3, 'red', 12],
                [1, 'red_backend', 4, 'RED', 13],
                [1, 'dark_blue_backend', 5, 'dark blue', 14],
                [1, 'blueish_backend', 6, 'blueish', 15],
                [1, 'blue_2_backend', 7, 'Blue2', 16],
                [1, 'black_backend', 2, 'black', 11]
            ],
            columns=[
                'attributeId', 'backendName', 'position', 'valueName',
                'valueId'
            ]
        )
        attr_service.dataframe = dataframe
        expectation = {
            'Blue': ['blue', 'Blue2', 'blueish'],
            'Red': ['RED', 'red']
        }
        assert attr_service.create_color_suggestions(
            colors=['Blue', 'Red']
        ) == expectation

    def with_too_long_excluded(fake_attribute_service) -> None:
        attr_service = fake_attribute_service
        dataframe = pandas.DataFrame(
            [
                [1, 'blue_backend', 1, 'blue', 10],
                [1, 'light_blue_backend', 3, 'light blue', 12],
                [1, 'matching_blue_backend', 4, 'Blue2', 13],
                [1, 'black_backend', 2, 'black', 11]
            ],
            columns=[
                'attributeId', 'backendName', 'position', 'valueName',
                'valueId'
            ]
        )
        attr_service.dataframe = dataframe
        expectation = {'Blue': ['blue', 'Blue2']}
        assert attr_service.create_color_suggestions(
            colors=['Blue']
        ) == expectation


def describe_get_data_for_flatfile():
    def with_empty_flatfile(fake_barcode_service):
        flatfile = pandas.DataFrame(
            [], columns=['item_sku', 'parent_child']
        )
        fake_barcode_service.dataframe = pandas.DataFrame(
            [
                ['1234', 'B012345678', 'X012345678'],
                ['2345', 'B012345679', 'X012345679'],
                ['3456', 'B012345680', 'X012345680']
            ], columns=['sku', 'asin', 'fnsku']
        )
        expectation = pandas.DataFrame(
            [], columns=['sku', 'asin', 'fnsku']
        )
        result = fake_barcode_service.get_data_for_flatfile(flatfile=flatfile)
        assert_frame_equal(result, expectation, check_index_type=False)

    def with_no_data_from_rest_api(fake_barcode_service):
        flatfile = pandas.DataFrame(
            [
                ['1230', 'parent'],
                ['1234', 'child'],
                ['2345', 'child']
            ], columns=['item_sku', 'parent_child']
        )
        fake_barcode_service.dataframe = pandas.DataFrame(
            [], columns=['sku', 'asin', 'fnsku']
        )
        expectation = pandas.DataFrame(
            [
                ['1234', '', ''],
                ['2345', '', '']
            ], columns=['sku', 'asin', 'fnsku']
        )
        result = fake_barcode_service.get_data_for_flatfile(flatfile=flatfile)
        assert_frame_equal(result, expectation)

    def with_all_found(fake_barcode_service):
        flatfile = pandas.DataFrame(
            [
                ['1230', 'parent'],
                ['1234', 'child'],
                ['2345', 'child'],
                ['3456', 'child']
            ], columns=['item_sku', 'parent_child']
        )
        fake_barcode_service.dataframe = pandas.DataFrame(
            [
                ['1234', 'B012345678', 'X012345678'],
                ['2345', 'B012345679', 'X012345679'],
                ['3456', 'B012345680', 'X012345680']
            ], columns=['sku', 'asin', 'fnsku']
        )
        expectation = pandas.DataFrame(
            [
                ['1234', 'B012345678', 'X012345678'],
                ['2345', 'B012345679', 'X012345679'],
                ['3456', 'B012345680', 'X012345680']
            ], columns=['sku', 'asin', 'fnsku']
        )
        result = fake_barcode_service.get_data_for_flatfile(flatfile=flatfile)
        assert_frame_equal(result, expectation)

    def with_no_found(fake_barcode_service):
        flatfile = pandas.DataFrame(
            [
                ['1230', 'parent'],
                ['4567', 'child'],
                ['5678', 'child']
            ], columns=['item_sku', 'parent_child']
        )
        fake_barcode_service.dataframe = pandas.DataFrame(
            [
                ['1234', 'B012345678', 'X012345678'],
                ['2345', 'B012345679', 'X012345679'],
                ['3456', 'B012345680', 'X012345680']
            ], columns=['sku', 'asin', 'fnsku']
        )
        expectation = pandas.DataFrame(
            [
                ['4567', '', ''],
                ['5678', '', '']
            ], columns=['sku', 'asin', 'fnsku']
        )
        result = fake_barcode_service.get_data_for_flatfile(flatfile=flatfile)
        assert_frame_equal(result, expectation)

    def with_partial_found(fake_barcode_service):
        flatfile = pandas.DataFrame(
            [
                ['1230', 'parent'],
                ['1234', 'child'],
                ['2345', 'child'],
                ['4567', 'child']
            ], columns=['item_sku', 'parent_child']
        )
        fake_barcode_service.dataframe = pandas.DataFrame(
            [
                ['1234', 'B012345678', 'X012345678'],
                ['2345', 'B012345679', 'X012345679'],
                ['3456', 'B012345680', 'X012345680']
            ], columns=['sku', 'asin', 'fnsku']
        )
        expectation = pandas.DataFrame(
            [
                ['4567', '', ''],
                ['1234', 'B012345678', 'X012345678'],
                ['2345', 'B012345679', 'X012345679']
            ], columns=['sku', 'asin', 'fnsku']
        )
        result = fake_barcode_service.get_data_for_flatfile(flatfile=flatfile)
        assert_frame_equal(result, expectation)

    def with_multiple_missing(fake_barcode_service):
        """
        Make sure that entries with empty values are at the top of the list
        sorted in ascending order by the SKU. Entries without missing barcodes
        are listed below in ascending order of the SKU
        """
        flatfile = pandas.DataFrame(
            [
                ['1230', 'parent'],
                ['1234', 'child'],
                ['2345', 'child'],
                ['4567', 'child'],
                ['5678', 'child'],
                ['12340', 'child'],
                ['test-sku', 'child']
            ], columns=['item_sku', 'parent_child']
        )
        fake_barcode_service.dataframe = pandas.DataFrame(
            [
                ['1234', 'B012345688', 'X012345688'],
                ['2345', 'B012345679', 'X012345679'],
                ['3456', 'B012345680', 'X012345680'],
                ['12340', 'B012345681', 'X012345681'],
                ['test-sku', 'B012345682', 'X012345682']
            ], columns=['sku', 'asin', 'fnsku']
        )
        expectation = pandas.DataFrame(
            [
                ['4567', '', ''],
                ['5678', '', ''],
                ['1234', 'B012345688', 'X012345688'],
                ['2345', 'B012345679', 'X012345679'],
                ['12340', 'B012345681', 'X012345681'],
                ['test-sku', 'B012345682', 'X012345682']
            ], columns=['sku', 'asin', 'fnsku']
        )
        result = fake_barcode_service.get_data_for_flatfile(flatfile=flatfile)
        assert_frame_equal(result, expectation)
